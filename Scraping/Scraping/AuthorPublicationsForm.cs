﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Scrapping.Classes;

namespace Scrapping
{
    public partial class AuthorPublicationsForm : Form
    {
        public AuthorPublicationsForm()
        {
            InitializeComponent();
        }

        public List<PublicationsOfAuthor> AuthorPublicationList
        {
            set { GridAuthPub.DataSource = value; }
        }
    }
}
