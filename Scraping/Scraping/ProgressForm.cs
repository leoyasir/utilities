﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Scrapping
{
    public partial class ProgressForm : Form
    {
        public ProgressBar ProgressBar { get { return progressBar; } }
        public string CancellingText { get; set; }
        public string DefaultStatusText { get; set; }

        public ProgressForm()
        {
            InitializeComponent();

            DefaultStatusText = "Please wait...";
        }

        public void SetProgress(int percent)
        {
            //do not update the progress bar if the value didn't change
            if (percent != lastPercent)
            {
                lastPercent = percent;
                progressBar.Value = percent;
            }
        }
        public void SetProgress(int percent, string status)
        {
            //update the form is at least one of the values need to be updated
            if (percent != lastPercent || (status != lastStatus))
            {
                lastPercent = percent;
                lastStatus = status;
                labelStatus.Text = status;
                progressBar.Value = percent;
            }
        }

        private void ProgressForm_Load(object sender, EventArgs e)
        {
            //reset to defaults just in case the user wants to reuse the form
            progressBar.Value = progressBar.Minimum;
            labelStatus.Text = DefaultStatusText;
            lastStatus = DefaultStatusText;
            lastPercent = progressBar.Minimum;
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            //disable the cancel button and change the status text
            labelStatus.Text = CancellingText;
        }

        int lastPercent;
        string lastStatus;
    }
}
