﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Scrapping
{
    public class CMessage
    {
        private static string mode;
        private static string action;
        private static string path;
        
        public static void ShowInformation(String strMessage)
        {
            MessageBox.Show(strMessage, "Expert Finder", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        public static void ShowError(String strMessage)
        {
            MessageBox.Show(strMessage, "Expert Finder", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        public static DialogResult ShowYesNo(String strMessage)
        {
            return MessageBox.Show(strMessage, "Expert Finder", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
        }

        private static ENUM_MESSAGE_TARGET message_target_;
        public enum ENUM_MESSAGE_TARGET
        {
            NONE = 0,
            STATUS_BAR = 1,
            MESSAGE_BOX = 2
        }
        public enum ENUM_MESEAGE_SEV
        {
            INFORMATION,
            WARNING,
            ERROR,
            FATAL
        }

        public CMessage()
        {
            message_target_ = ENUM_MESSAGE_TARGET.MESSAGE_BOX;
        }
        static CMessage()
        {
            message_target_ = ENUM_MESSAGE_TARGET.MESSAGE_BOX;
        }

        private static void DialogMessage(String strMessage, ENUM_MESEAGE_SEV ms)
        {
            MessageBoxIcon msgicon = MessageBoxIcon.Question;
            switch (ms)
            {
                case ENUM_MESEAGE_SEV.INFORMATION:
                    msgicon = MessageBoxIcon.Information;
                    break;
                case ENUM_MESEAGE_SEV.WARNING:
                    msgicon = MessageBoxIcon.Warning;
                    break;
                case ENUM_MESEAGE_SEV.ERROR:
                    msgicon = MessageBoxIcon.Error;
                    break;
                case ENUM_MESEAGE_SEV.FATAL:
                    msgicon = MessageBoxIcon.Stop;
                    break;
                default:
                    break;
            }
            MessageBox.Show(strMessage, "Expert Finder", MessageBoxButtons.OK, msgicon);
        }

        private static void StatusBarMessage(String strMessage, ENUM_MESEAGE_SEV ms)
        {

        }

        public static void ShowMessage(String strMessage, ENUM_MESEAGE_SEV ms)
        {
            if (message_target_ == ENUM_MESSAGE_TARGET.MESSAGE_BOX)
                DialogMessage(strMessage, ms);
            if (message_target_ == ENUM_MESSAGE_TARGET.STATUS_BAR)
                StatusBarMessage(strMessage, ms);
        }

        public static void ShowErrorMessage(String strMessage)
        {
            DialogMessage(strMessage, ENUM_MESEAGE_SEV.ERROR);
        }

        public static string Mode
        {
            get { return mode; }
            set { mode = value; }
        }

        public static string Action
        {
            get { return action; }
            set { action = value; }
        }

        public static string Path
        {
            get { return path; }
            set { path = value; }
        }
    }
}
