﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Scrapping;

namespace Scrapping.Classes
{
    public class CoAuthor_Helper
    {
        private Categories _Category;

        public CoAuthor_Helper()
        {
            _Category = null;
        }

        public List<AuthorsByCoAuthors> AuthorsCoAuthors
        {
            get
            {
                List<AuthorsByCoAuthors> CoAuthorsList = new List<AuthorsByCoAuthors>();
                try
                {
                    DataRowCollection CoAuthorDataRowCollection;
                    CDatabase db = new CDatabase(false);
                    DataSet CoAuthorDS;
                    db.BindInParameter("@category_id", SelectedCategory.CategoryID, SqlDbType.Int);
                    CoAuthorDS = db.ExecuteDataSetSP("sp_Authors_By_CoAuthors");
                    if (CoAuthorDS != null)
                    {
                        CoAuthorDataRowCollection = CoAuthorDS.Tables[0].Rows;
                        CoAuthorsList.Clear();
                        foreach (DataRow AuthorDataRow in CoAuthorDataRowCollection)
                        {
                            CoAuthorsList.Add(new AuthorsByCoAuthors(AuthorDataRow));
                            CoAuthorsList.Last<AuthorsByCoAuthors>().CategoryID = SelectedCategory.CategoryID;
                        }
                    }
                    db.CloseDbConnection();
                }
                catch (Exception dbEx)
                {
                    CMessage.ShowError(dbEx.Message);
                    return null;
                }
                return CoAuthorsList;
            }
        }

        public Categories SelectedCategory
        {
            get { return _Category; }
            set { _Category = value; }
        }
    }
}
