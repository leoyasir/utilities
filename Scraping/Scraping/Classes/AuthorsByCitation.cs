﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Scrapping;

namespace Scrapping.Classes
{
    public class AuthorsByCitation
    {
        STAuthor _Author;
        int _CitationCount;

        public AuthorsByCitation()
        {
            _Author.AuthorID = -1;
            _Author.AuthorFName = "";
            _Author.AuthorLName = "";
            _CitationCount = 0;
        }

        public AuthorsByCitation(DataRow AuthorDataRow): this()
        {
            _Author.AuthorID = CDatabase.GetIntFromDataRow(AuthorDataRow, "author_id");
            _Author.AuthorFName = CDatabase.GetStringFromDataRow(AuthorDataRow, "author_first_name");
            _Author.AuthorLName = CDatabase.GetStringFromDataRow(AuthorDataRow, "author_last_name");
            _CitationCount = CDatabase.GetIntFromDataRow(AuthorDataRow, "citation_count");
        }

        public int AuthorID
        {
            get { return _Author.AuthorID; }
            set { _Author.AuthorID = value; }
        }

        public string AuthorName
        {
            get { return _Author.AuthorName; }
        }

        public int CitationCount
        {
            get { return _CitationCount; }
            set { _CitationCount = value; }
        }
    }
}
