﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Scrapping;

namespace Scrapping.Classes
{
    public class Category_Expert
    {
        STAuthor _Author;
        float _PublicationWeight;
        float _CitationsWeight;
        float _CoAuthorWeight;
        int _CategoryID;
        string _CategoryName;
        int _hindex;

        public Category_Expert()
        {
            _Author.AuthorID = -1;
            _PublicationWeight = 0;
            _CitationsWeight = 0;
            _CoAuthorWeight = 0;
            _Author.AuthorFName = "";
            _Author.AuthorLName = "";
            _CategoryID = -1;
            _CategoryName = "";
            _hindex = 0;
        }

        public Category_Expert(DataRow ExpertDataRow): this()
        {
            _Author.AuthorID = CDatabase.GetIntFromDataRow(ExpertDataRow, "author_id");
            _PublicationWeight = CDatabase.GetFloatFromDataRow(ExpertDataRow, "publications_weight");
            _CitationsWeight = CDatabase.GetFloatFromDataRow(ExpertDataRow, "citations_weight");
            _CoAuthorWeight = CDatabase.GetFloatFromDataRow(ExpertDataRow, "coAuthor_weight");
            _Author.AuthorFName = CDatabase.GetStringFromDataRow(ExpertDataRow, "author_first_name");
            _Author.AuthorLName = CDatabase.GetStringFromDataRow(ExpertDataRow, "author_last_name");
            _CategoryID = CDatabase.GetIntFromDataRow(ExpertDataRow, "category_id");
            _CategoryName = CDatabase.GetStringFromDataRow(ExpertDataRow, "category_name");
            _hindex = CDatabase.GetIntFromDataRow(ExpertDataRow, "hindex");
        }

        public int AuthorID
        {
            get { return _Author.AuthorID; }
            set { _Author.AuthorID = value; }
        }

        //public string AuthorFName
        //{
        //    get { return _AuthorFName; }
        //    set { _AuthorFName = value; }
        //}

        //public string AuthorLName
        //{
        //    get { return _AuthorLName; }
        //    set { _AuthorLName = value; }
        //}

        public string AuthorName
        {
            get { return _Author.AuthorName; }
        }

        public string PublicationWeight
        {
            get { return _PublicationWeight.ToString("0.####"); }
            set { _PublicationWeight = float.Parse(value); }
        }

        public string CitationWeight
        {
            get { return _CitationsWeight.ToString("0.####"); }
            set { _CitationsWeight = float.Parse(value); }
        }

        public string CoAuthorWeight
        {
            get { return _CoAuthorWeight.ToString("0.####"); }
            set { _CoAuthorWeight = float.Parse(value); }
        }

        public int CategoryID
        {
            get { return _CategoryID; }
            set { _CategoryID = value; }
        }

        public string CategoryName
        {
            get { return _CategoryName; }
            set { _CategoryName = value; }
        }

        public string TotalWeight
        {
            get { return (_PublicationWeight + _CitationsWeight + _CoAuthorWeight).ToString("0.###"); }
        }

        public int HIndex
        {
            get { return _hindex; }
        }
    }
}
