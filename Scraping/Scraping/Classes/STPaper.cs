﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Scrapping.Classes
{
    public struct STPaper
    {
        public int PaperID;
        public string PaperTitle;
        public string PaperURL;
        public string Conference;
        public string DownloadLink;
        public int CitationCount;
        public string SearchEngine;
        private STCategory Category;

        public int CategoryID
        {
            get { return Category.CategoryID; }
            set { Category.CategoryID = value; }
        }

        public string CategoryName
        {
            get { return Category.CategoryName; }
            set { Category.CategoryName = value; }
        }
    }
}
