﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Scrapping;

namespace Scrapping.Classes
{
    public class Categories
    {
        private int _categoryID;
        private string _categoryName;

        public Categories()
        {
            _categoryID = -1;
            _categoryName = "";
        }

        public Categories(DataRow CategoryDataRow): this()
        {
            _categoryID = CDatabase.GetIntFromDataRow(CategoryDataRow, "category_id");
            _categoryName = CDatabase.GetStringFromDataRow(CategoryDataRow, "category_name");
        }

        public int CategoryID
        {
            get { return _categoryID; }
            set { _categoryID = value; }
        }

        public string CategoryName
        {
            get { return _categoryName; }
            set { _categoryName = value; }
        }

        public override string ToString()
        {
            return CategoryName;
        }

        public static List<Categories> DBCategories
        {
            get
            {
                List<Categories> CategoriesList = new List<Categories>();
                try
                {
                    DataRowCollection CategoryDataRowCollection;
                    CDatabase db = new CDatabase(false);
                    DataSet CategoryDS;
                    //db.BindInParameter("@author_id", SelectedAuthor.AuthorID, SqlDbType.Int);
                    CategoryDS = db.ExecuteDataSetSP("sp_Get_Categories");
                    if (CategoryDS != null)
                    {
                        CategoryDataRowCollection = CategoryDS.Tables[0].Rows;
                        CategoriesList.Clear();
                        foreach (DataRow CategoryDataRow in CategoryDataRowCollection)
                        {
                            CategoriesList.Add(new Categories(CategoryDataRow));
                        }
                    }
                    db.CloseDbConnection();
                }
                catch (Exception dbEx)
                {
                    CMessage.ShowError(dbEx.Message);
                    return null;
                }
                return CategoriesList;
            }
        }
    }
}
