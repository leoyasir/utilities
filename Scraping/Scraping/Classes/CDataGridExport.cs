﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Excel = Microsoft.Office.Interop.Excel;
using Microsoft.Office.Interop.Excel;
using System.Windows.Forms;
using System.IO;

namespace Scrapping.Classes
{
    public class CDataGridExport
    {
        private object missingValue;
        String strDateTime = "";
        public String strFilePath = "";
        private Excel.Application xlApp_;
        private Excel.Workbook xlWorkBook_;
        public CDataGridExport()
        {
            missingValue = System.Reflection.Missing.Value;
            strDateTime = DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Day.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + DateTime.Now.Second.ToString();
            strFilePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Desktop), strDateTime + "Data.xls");
        }

        public void initializeExcelApplication()
        {
            this.xlApp_ = new Excel.ApplicationClass();
            this.xlWorkBook_ = this.xlApp_.Workbooks.Add(missingValue);
            this.deleteAllWorkSheets();
        }

        private void deleteAllWorkSheets()
        {
            Excel.Worksheet xlWorkSheet;
            for (int i = 0; i < 2; i++)
            {
                xlWorkSheet = (Excel.Worksheet)this.xlWorkBook_.Worksheets.get_Item(1);
                xlWorkSheet.Delete();
            }
        }

        public Excel.Worksheet getExistingWorkSheet(String strTitle)
        {
            Excel.Worksheet xlWorkSheet;
            xlWorkSheet = (Excel.Worksheet)this.xlWorkBook_.Worksheets.get_Item(1);
            xlWorkSheet.Name = strTitle;
            return xlWorkSheet;
        }

        public Excel.Worksheet addNewExcelWorkSheet(String strTitle, int nSheetIndex)
        {
            Excel.Worksheet xlWorkSheet;
            if (nSheetIndex == 0)
            {
                xlWorkSheet = (Excel.Worksheet)this.xlWorkBook_.Worksheets.get_Item(1);
            }
            else
            {
                xlWorkSheet = (Excel.Worksheet)this.xlWorkBook_.Worksheets.Add(missingValue, missingValue, missingValue, missingValue);                
            }
            xlWorkSheet.Name = strTitle;
            return xlWorkSheet;
        }

        public void writeDataGridIntoExcel(Excel.Worksheet xlWorkSheet, DataGridView dgDataGrid)
        {
            Int16 i, j;
            String strValue = "";
            for (i = 0; i <= dgDataGrid.RowCount - 1; i++)
            {
                xlWorkSheet.get_Range(xlWorkSheet.Cells[i + 1, 1], xlWorkSheet.Cells[i + 1, 1]).EntireRow.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                for (j = 0; j <= dgDataGrid.ColumnCount - 1; j++)
                {
                    if (dgDataGrid[j, i].Value != null)
                    {
                        strValue = dgDataGrid[j, i].Value.ToString();
                    }
                    else
                    {
                        strValue = "";
                    }
                    if (i == 0)
                    {
                        xlWorkSheet.Cells[i + 1, j + 1] = dgDataGrid.Columns[j].HeaderText;
                        xlWorkSheet.get_Range(xlWorkSheet.Cells[i + 1, j + 1], xlWorkSheet.Cells[i + 1, j + 1]).Font.Bold = true;
                        xlWorkSheet.Cells[i + 2, j + 1] = strValue;
                    }
                    else
                    {
                        xlWorkSheet.Cells[i + 2, j + 1] = strValue;
                    }
                }
            }
            xlWorkSheet.get_Range(xlWorkSheet.Cells[i + 1, 1], xlWorkSheet.Cells[i + 1, 1]).EntireRow.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
        }

        public void saveExcelApplication()
        {
            this.xlWorkBook_.SaveAs(strFilePath, Excel.XlFileFormat.xlWorkbookNormal, missingValue, missingValue, missingValue, missingValue, Excel.XlSaveAsAccessMode.xlExclusive, missingValue, missingValue, missingValue, missingValue, missingValue);
            this.xlWorkBook_.Close(true, missingValue, missingValue);
            this.xlApp_.Quit();            
        }

        public void quitWithoutSave()
        {
            this.xlWorkBook_.Close(false, missingValue, missingValue);
            this.xlApp_.Quit();
        }

        public void releaseApplicationObject()
        {
            this.releaseObject(this.xlWorkBook_);
            this.releaseObject(this.xlApp_);
        }
        
        public void releaseObject(object obj)
        {
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
                obj = null;
            }

            catch (Exception ex)
            {
                obj = null;
                MessageBox.Show("Exception Occured while releasing object " + ex.ToString());
            }

            finally
            {
                GC.Collect();
            }
        }
    }
}
