﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Scrapping;

namespace Scrapping.Classes
{
    public class AuthorsByPublications
    {
        STAuthor _Author;
        private int _numberOfPub;

        public AuthorsByPublications()
        {
            _Author.AuthorID = -1;
            _Author.AuthorFName = "";
            _Author.AuthorLName = "";
            _numberOfPub = 0;
        }

        public AuthorsByPublications(DataRow AuthorDataRow): this()
        {
            _Author.AuthorID = CDatabase.GetIntFromDataRow(AuthorDataRow, "author_id");
            _Author.AuthorFName = CDatabase.GetStringFromDataRow(AuthorDataRow, "author_first_name");
            _Author.AuthorLName = CDatabase.GetStringFromDataRow(AuthorDataRow, "author_last_name");
            _numberOfPub = CDatabase.GetIntFromDataRow(AuthorDataRow, "publication_count");
        }

        public int AuthorID
        {
            get { return _Author.AuthorID; }
            set { _Author.AuthorID = value; }
        }

        public string AuthorName
        {
            get { return _Author.AuthorName; }
        }

        public int NumberOfPublications
        {
            get { return _numberOfPub; }
            set { _numberOfPub = value; }
        }
    }
}
