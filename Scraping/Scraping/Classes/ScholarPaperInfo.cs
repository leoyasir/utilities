﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Scrapping;
using System.Data.SqlClient;

namespace Scrapping.Classes
{
    public class ScholarPaperInfo
    {
        private string _title;
        private string _url;
        private string _authors;
        private string _conference;
        private int _cite_count;
        private string downloadLink;
        private STCategory category;
        private string searchEngine;
        private string authorString;

        public ScholarPaperInfo()
        {
            _title = "";
            _url = "";
            authorString = _authors = "";
            _conference = "";
            _cite_count = 0;
            downloadLink = "";
            searchEngine = "Scholar";
        }

        public string Title
        {
            get { return _title; }
            set { _title = value; }
        }

        public string Url
        {
            get { return _url; }
            set { _url = value; }
        }

        public string Authors
        {
            get { return _authors; }
            set
            {
                _authors = value;
                authorString = "";

                string[] splitChars = { "," };
                string[] auths = _authors.Split(splitChars, StringSplitOptions.RemoveEmptyEntries);
                
                if (searchEngine == "Scholar")
                { 
                    foreach (string author in auths)
                    {
                        string[] splitChars1 = { " " };
                        string[] authsPart = author.Split(splitChars1, StringSplitOptions.RemoveEmptyEntries);
                        foreach (string subName in authsPart)
                        {
                            if (subName.EndsWith("."))
                                subName.Remove(subName.Length - 1);

                            authorString += "^" + subName;
                        }
                        authorString += ",";
                    }
                }
                else if (searchEngine == "PubMed")
                {
                    foreach (string author in auths)
                    {
                        string[] splitChars1 = { " " };
                        string[] authsPart = author.Split(splitChars1, StringSplitOptions.RemoveEmptyEntries);
                        for (int i = authsPart.Length -1; i >= 0;i-- )
                        {
                            if (authsPart[i].EndsWith("."))
                                authsPart[i] = authsPart[i].Remove(authsPart[i].Length - 1);

                            authorString += "^" + authsPart[i];
                        }
                        authorString += ",";
                    }
                }
            }
        }

        public string Conference
        {
            get { return _conference; }
            set { _conference = value; }
        }

        public int CitationsCount
        {
            get { return _cite_count; }
            set { _cite_count = value; }
        }

        public string DownloadLink
        {
            get { return downloadLink; }
            set { downloadLink = value; }
        }

        public string Category
        {
            get { return category.CategoryName; }
            set { category.CategoryName = value; }
        }

        public string AuthorEngine
        {
            get { return searchEngine; }
            set { searchEngine = value; }
        }

        public bool Save()
        {
            try
            {
                CDatabase db = new CDatabase();
                db.BindInParameter("@paper_title", _title, System.Data.SqlDbType.VarChar);
                db.BindInParameter("@paper_url", _url, System.Data.SqlDbType.VarChar);
                db.BindInParameter("@conference", _conference, System.Data.SqlDbType.VarChar);
                db.BindInParameter("@download_link", downloadLink, System.Data.SqlDbType.VarChar);
                db.BindInParameter("@citation_count", _cite_count, System.Data.SqlDbType.Int);
                db.BindInParameter("@category", category.CategoryName, System.Data.SqlDbType.VarChar);
                db.BindInParameter("@authors", authorString, System.Data.SqlDbType.VarChar);
                db.BindInParameter("@paper_engine", searchEngine, System.Data.SqlDbType.VarChar);

                db.ExecuteNonQuery("sp_Insert_Paper_Info");

                db.CloseDbConnection();
            }
            catch (Exception e)
            {
                CMessage.ShowErrorMessage(e.ToString());
            }
            return true;
        }
    }
}
