﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace Scrapping.Classes
{
    public class Paper_Helper
    {
        public static List<AuthorsByPublications> CountPublicationsByAuthors
        {
            get
            {
                List<AuthorsByPublications> PublicationList = new List<AuthorsByPublications>();
                try
                {
                    DataRowCollection AuthorsDataRowCollection;
                    CDatabase db = new CDatabase(false);
                    DataSet AuthorsDS;
                    AuthorsDS = db.ExecuteDataSetSP("sp_Number_Of_Publications");
                    if (AuthorsDS != null)
                    {
                        AuthorsDataRowCollection = AuthorsDS.Tables[0].Rows;
                        PublicationList.Clear();
                        foreach (DataRow AuthorDataRow in AuthorsDataRowCollection)
                        {
                            PublicationList.Add(new AuthorsByPublications(AuthorDataRow));
                        }
                    }
                    db.CloseDbConnection();
                }
                catch (Exception dbEx)
                {
                    CMessage.ShowError(dbEx.Message);
                    return null;
                }
                return PublicationList;
            }
        }
    }
}
