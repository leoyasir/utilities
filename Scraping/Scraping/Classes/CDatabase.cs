﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using Microsoft.ApplicationBlocks.Data;

namespace Scrapping
{
    public class CDatabase
    {
        static String strDatabaseName;
        static String strDatabaseServer;

        //  COMMENTS
        // When we use string from the app.config file the designer views in which database access is used, gives exception
        // and the design does not appear correctly. But actually when we run the application it works fine. So in order to
        // overcome this problem we use hardcoded string for testing purposes. Also the QueryTimeout and ConnTimeout should
        // be replaced with 240 or other desireable value

        //Use these lines for deployment
        //private string strFromConfig = System.Configuration.ConfigurationSettings.AppSettings["DBConString"];
        //private string strPasswordFromConfig = System.Configuration.ConfigurationSettings.AppSettings["DBPassword"];
        //private string ConnTimeout = System.Configuration.ConfigurationSettings.AppSettings["ConnTimeout"];
        //private string QueryTimeout = System.Configuration.ConfigurationSettings.AppSettings["QueryTimeout"];
        //private String strFromConfig = ConfigurationManager.ConnectionStrings["localString"].ConnectionString;

        /* Open these lines during coding so that designer will not give any exception*/
        private string ConnTimeout = "240";
        private string QueryTimeout = "480";
         /**/

        private String strLocal = "Server=182.180.80.94,5999;Database=BioInformatics;user id=sa;password=wwcpw786; TimeOut=480";
        //private String strLocal = "Server=203.82.54.44\\SQLEXPRESS;Database=TeleDB;user id=sa;password=MacBookPro7374; TimeOut=240";
        //private String strLocal = "Server=115.186.175.184;Database=TeleDB;user id=sa;password=MacBookPro7374; TimeOut=240";
        //private String strLocal = "Server=115.186.175.186,5999;Database=TeleDB;user id=sa;password=MacBookPro7374; TimeOut=240";
        //private String strLocal = "Server=192.168.2.111\\SQLEXPRESS;Database=TeleDB;user id=sa;password=abc; TimeOut=240";
        private SqlConnection sqlConnection_ = null;
        private SqlDataReader sqlDataReader_ = null;
        List<SqlParameter> sqlParameters = null;
        private SqlTransaction Transaction_ = null;
        bool bindLoginId_;
        static bool DependencyInitialized = false;
        private static string strConnectionString = "";
        public event EventHandler OnDbDataChanged;
        public CDatabase()
            : this(true)
        {
        }
        public static String ConnectionString
        {
            get
            {
                return strConnectionString;
            }
        }
        public CDatabase(bool bindLoginId)
        {
            /*** Close these lines during coding to avoid license checking ***/

            //if (CSession.bUseLicense)
            //{
            //    CLicenseManager LicenseManager = CLicenseManager.GetLicenseManager();
            //    sqlConnection_ = new SqlConnection(LicenseManager.ConnectionString); //Password is Decrypted in LicenseManager File 
            //}
            //else
            //{
            //    if ((strFromConfig == null) || (strPasswordFromConfig == null))
            //        throw new Exception("Invalid databse connection parameters");
            //    String strDecrypted = strFromConfig + "password=" + CLisence.Decrypt(strPasswordFromConfig); //Password is Decrypted in here in this File
            //    strDecrypted = strDecrypted + "; TimeOut=" + ConnTimeout;
            //    sqlConnection_ = new SqlConnection(strDecrypted);
            //}
            /***/

            //Open this line for user contrls designer. 
            sqlConnection_ = new SqlConnection(strLocal);

            if (DependencyInitialized == false)
            {
                strConnectionString = sqlConnection_.ConnectionString;
                strDatabaseServer = sqlConnection_.DataSource;
                strDatabaseName = sqlConnection_.Database;
                EnoughPermission();
                SqlDependency.Start(strConnectionString);
                DependencyInitialized = true;
            }

            sqlParameters = new List<SqlParameter>();
            bindLoginId_ = bindLoginId;
        }

        public static String DatabaseName
        {
            get { return strDatabaseName; }
        }

        public static String DatabaseServer
        {
            get { return strDatabaseServer; }
        }


        ~CDatabase()
        {
            try
            {
                if (sqlConnection_.State == ConnectionState.Open)
                    sqlConnection_.Close();
            }
            catch (Exception)
            {
            }
        }
        public bool BeginTransaction()
        {
            if (Transaction_ == null)
            {
                Transaction_ = sqlConnection_.BeginTransaction();
                return true;
            }
            return false;
        }

        public bool CommitTransaction()
        {
            if (Transaction_ != null)
            {
                Transaction_.Commit();
                return true;
            }
            return false;
        }

        public bool RollbackTransaction()
        {
            if (Transaction_ != null)
            {
                Transaction_.Rollback();
                return true;
            }
            return false;
        }

        public void OpenDbConnection()
        {
            if (sqlConnection_.State == ConnectionState.Closed)
                sqlConnection_.Open();
        }

        public bool CloseDbConnection()
        {
            try
            {
                if (sqlConnection_.State == ConnectionState.Open)
                    sqlConnection_.Close();
            }
            catch (Exception e)
            {
                CMessage.ShowErrorMessage(e.Message);
                return false;
            }
            return true;
        }

        public SqlDataReader ExecuteReaderSP(String strSP)
        {
            try
            {
                OpenDbConnection();
                //sqlDataReader_ = SqlHelper.ExecuteReader(sqlConnection_, CommandType.StoredProcedure, strSP, sqlParameters.ToArray());
                SqlCommand cmd;
                cmd = new SqlCommand(strSP, sqlConnection_);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Notification = null;
                cmd.CommandTimeout = Int32.Parse(QueryTimeout);
                cmd.Parameters.AddRange(sqlParameters.ToArray());
                sqlDataReader_ = cmd.ExecuteReader();
                return sqlDataReader_;

            }
            catch (Exception e)
            {
                CMessage.ShowError(e.Message);
            }
            return null;
        }
        private bool EnoughPermission()
        {

            SqlClientPermission perm = new SqlClientPermission(System.Security.Permissions.PermissionState.Unrestricted);
            try
            {
                perm.Demand();
                return true;
            }
            catch (System.Exception)
            {
                return false;
            }
        }
        public DataSet ExecuteDependencyDataSetSP(String strSP)
        {
            SqlCommand cmd;
            DataSet Ds = null;
            OpenDbConnection();
            cmd = new SqlCommand(strSP, sqlConnection_);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Notification = null;
            cmd.CommandTimeout = Int32.Parse(QueryTimeout);
            cmd.Parameters.AddRange(sqlParameters.ToArray());
            SqlDependency dependency = new SqlDependency(cmd);

            dependency.OnChange += new OnChangeEventHandler(OnChange);

            SqlDataAdapter DataAdapter = new SqlDataAdapter(cmd);
            Ds = new DataSet();
            DataAdapter.Fill(Ds);
            //OpenDbConnection();
            //            Ds = SqlHelper.ExecuteDataset(sqlConnection_, CommandType.StoredProcedure, strSP, sqlParameters.ToArray());
            ClearParameterArray();
            return Ds;
        }

        void OnChange(object sender, SqlNotificationEventArgs e)
        {
            SqlDependency dependency = sender as SqlDependency;
            // Notices are only a one shot deal so remove the existing one so a new one can be added
            dependency.OnChange -= OnChange;

            if (e.Info == SqlNotificationInfo.Insert ||
                e.Info == SqlNotificationInfo.Delete ||
                e.Info == SqlNotificationInfo.Update)
            {
                // Fire the event
                if (OnDbDataChanged != null)
                    OnDbDataChanged(this, EventArgs.Empty);
            }
            //What to do otherwise
        }

        public DataSet ExecuteDataSetSP_(String strSP)
        {
            DataSet Ds = null;
            try
            {
                OpenDbConnection();
                Ds = SqlHelper.ExecuteDataset(sqlConnection_, CommandType.StoredProcedure, strSP, sqlParameters.ToArray());
                ClearParameterArray();
            }
            catch (Exception DbEx)
            {
                String str = "Database Server Error" + Environment.NewLine;
                str += "Details:" + Environment.NewLine;
                str += DbEx.Message;
                CMessage.ShowError(str);
            }
            return Ds;
        }

        public DataSet ExecuteDataSetSP(String strSP)
        {
            try
            {
                SqlCommand com = new SqlCommand();
                PrepareCommand(com, null, CommandType.StoredProcedure, strSP, sqlParameters.ToArray());
                using (SqlDataAdapter da = new SqlDataAdapter(com))
                {
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    ClearParameterArray();
                    return ds;
                }
            }
            catch (Exception exc)
            {
                if (exc.Message.Contains("DMDEV"))
                {
                    CMessage.ShowError("Cannot Connect to the remote database server.");
                    return null;
                }
                else
                    throw exc;
            }
        }

        public void ExecuteNonQuery_(String strSP)
        {
            OpenDbConnection();
            if (Transaction_ == null)
                SqlHelper.ExecuteNonQuery(sqlConnection_, CommandType.StoredProcedure, strSP, sqlParameters.ToArray());
            else
                SqlHelper.ExecuteNonQuery(Transaction_, CommandType.StoredProcedure, strSP, sqlParameters.ToArray());
            ClearParameterArray();
        }

        public void ExecuteNonQuery(String strSP)
        {
            OpenDbConnection();
            try
            {
                SqlCommand com = new SqlCommand();
                PrepareCommand(com, null, CommandType.StoredProcedure, strSP, sqlParameters.ToArray());
                using (SqlDataAdapter da = new SqlDataAdapter(com))
                {
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                }
            }
            catch (Exception exc)
            {
                throw exc;
            }
            ClearParameterArray();
        }

        private void PrepareCommand(SqlCommand cmd, SqlTransaction trans, CommandType cmdType, string cmdText, SqlParameter[] cmdParms)
        {
            cmd.Connection = sqlConnection_;
            cmd.CommandText = cmdText;

            if (trans != null)
                cmd.Transaction = trans;

            cmd.CommandType = cmdType;
            cmd.CommandTimeout = Int32.Parse(QueryTimeout);

            if (cmdParms != null)
            {
                foreach (SqlParameter parm in cmdParms)
                    cmd.Parameters.Add(parm);
            }
            return;
        }

        #region BindParameters
        public bool BindInParameter(String strParam, Object Value, SqlDbType sqlType, int size)
        {
            SqlParameter sqlParameter;
            sqlParameter = new SqlParameter(strParam, sqlType, size);
            sqlParameter.Value = Value;
            sqlParameter.Direction = ParameterDirection.Input;
            //sqlParameter.
            sqlParameters.Add(sqlParameter);
            return true;
        }
        public bool BindInParameter(String strParam, Object Value, SqlDbType sqlType)
        {
            return BindInParameter(strParam, Value, sqlType, 0);
        }

        public SqlParameter BindOutParameter(String strParam, SqlDbType sqlType, int size)
        {
            SqlParameter sqlParameter;
            sqlParameter = new SqlParameter(strParam, sqlType, size);
            sqlParameter.Direction = ParameterDirection.Output;
            sqlParameters.Add(sqlParameter);
            return sqlParameter;
        }

        public SqlParameter BindInOutParameter(String strParam, Object Value, SqlDbType sqlType, int size)
        {
            SqlParameter sqlParameter;
            sqlParameter = new SqlParameter(strParam, sqlType, size);
            sqlParameter.Value = Value;
            sqlParameter.Direction = ParameterDirection.InputOutput;
            sqlParameters.Add(sqlParameter);
            return sqlParameter;
        }

        public SqlParameter BindOutParameter(String strParam, SqlDbType sqlType)
        {
            return BindOutParameter(strParam, sqlType, 0);
        }
        private void ClearParameterArray()
        {
            sqlParameters = null;
            sqlParameters = new List<SqlParameter>();
        }
        #endregion

        #region DataSet Get Data Function
        public static object GetObjectFromDataRow(DataRow PatientDataRow, String Column)
        {
            object obj;
            if (DBNull.Value != (obj = PatientDataRow[Column]))
            {
                return obj;
            }
            return null;
        }

        public static String GetStringFromDataRow(DataRow PatientDataRow, String Column)
        {
            object obj;
            obj = GetObjectFromDataRow(PatientDataRow, Column);
            if (obj != null)
                return (String)obj;
            return "";
        }

        public static int GetIntFromDataRow(DataRow PatientDataRow, String Column)
        {
            object obj;
            obj = GetObjectFromDataRow(PatientDataRow, Column);
            if (obj != null)
                return Convert.ToInt32(obj);
            return 0;
        }

        public static bool GetBoolFromDataRow(DataRow PatientDataRow, String Column)
        {
            object obj;
            obj = GetObjectFromDataRow(PatientDataRow, Column);
            if (obj != null)
                return (bool)obj;
            return false;
        }

        public static float GetFloatFromDataRow(DataRow PatientDataRow, String Column)
        {
            object obj;
            obj = GetObjectFromDataRow(PatientDataRow, Column);
            if (obj != null)
                return Convert.ToSingle(obj);
            return 0;
        }

        public static char GetCharFromDataRow(DataRow PatientDataRow, String Column)
        {
            object obj;
            obj = GetObjectFromDataRow(PatientDataRow, Column);
            if (obj != null)
                return (Char)obj.ToString()[0];
            return ' ';
        }

        public static DateTime GetDateTimeFromDataRow(DataRow PatientDataRow, String Column)
        {
            DateTime dt;
            object obj;
            dt = DateTime.MinValue;
            obj = GetObjectFromDataRow(PatientDataRow, Column);
            if (obj != null)
                dt = (DateTime)obj;
            return dt;
        }

        public static decimal GetDecimalFromDataRow(DataRow PatientDataRow, String Column)
        {
            object obj;
            obj = GetObjectFromDataRow(PatientDataRow, Column);
            if (obj != null)
            {
                return (decimal)PatientDataRow[Column];

            }
            return -1m;
        }
        #endregion
    }
}
