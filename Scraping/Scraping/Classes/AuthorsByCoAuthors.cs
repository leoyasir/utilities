﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Scrapping;

namespace Scrapping.Classes
{
    public class AuthorsByCoAuthors
    {
        STAuthor _Author;
        int _CoAuthorsCount;
        int _CategoryID;

        public AuthorsByCoAuthors()
        {
            _Author.AuthorID = -1;
            _Author.AuthorFName = "";
            _Author.AuthorLName = "";
            _CoAuthorsCount = 0;
            _CategoryID = 0;
        }

        public AuthorsByCoAuthors(DataRow AuthorDataRow): this()
        {
            _Author.AuthorID = CDatabase.GetIntFromDataRow(AuthorDataRow, "author_id");
            _Author.AuthorFName = CDatabase.GetStringFromDataRow(AuthorDataRow, "author_first_name");
            _Author.AuthorLName = CDatabase.GetStringFromDataRow(AuthorDataRow, "author_last_name");
            _CoAuthorsCount = CDatabase.GetIntFromDataRow(AuthorDataRow, "coauthors_count");
        }

        public int AuthorID
        {
            get { return _Author.AuthorID; }
            set { _Author.AuthorID = value; }
        }

        public string AuthorName
        {
            get { return _Author.AuthorName; }
        }

        public int CoAuthorCount
        {
            get { return _CoAuthorsCount; }
            set { _CoAuthorsCount = value; }
        }

        public int CategoryID
        {
            get { return _CategoryID; }
            set { _CategoryID = value; }
        }
    }
}
