﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Scrapping;

namespace Scrapping.Classes
{
    public class Citation_Helper
    {
        private Categories _Category;

        public Citation_Helper()
        {
            _Category = null;
        }

        public List<AuthorsByCitation> AuthorsCitations
        {
            get
            {
                List<AuthorsByCitation> CitationList = new List<AuthorsByCitation>();
                try
                {
                    DataRowCollection CitationDataRowCollection;
                    CDatabase db = new CDatabase(false);
                    DataSet CitationDS;
                    db.BindInParameter("@category_id", SelectedCategory.CategoryID, SqlDbType.Int);
                    CitationDS = db.ExecuteDataSetSP("sp_Authors_By_Citations");
                    if (CitationDS != null)
                    {
                        CitationDataRowCollection = CitationDS.Tables[0].Rows;
                        CitationList.Clear();
                        foreach (DataRow AuthorDataRow in CitationDataRowCollection)
                        {
                            CitationList.Add(new AuthorsByCitation(AuthorDataRow));
                        }
                    }
                    db.CloseDbConnection();
                }
                catch (Exception dbEx)
                {
                    CMessage.ShowError(dbEx.Message);
                    return null;
                }
                return CitationList;
            }
        }

        public Categories SelectedCategory
        {
            get { return _Category; }
            set { _Category = value; }
        }
    }
}
