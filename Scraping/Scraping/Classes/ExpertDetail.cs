﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Scrapping;

namespace Scrapping.Classes
{
    public class ExpertDetail
    {
        private STAuthor _Author;
        private STPaper _Paper;

        public ExpertDetail()
        { 
        }

        public ExpertDetail(DataRow ExpertDataRow): this()
        {
            _Author.AuthorID = CDatabase.GetIntFromDataRow(ExpertDataRow, "author_id");
            _Author.AuthorFName = CDatabase.GetStringFromDataRow(ExpertDataRow, "author_first_name");
            _Author.AuthorLName = CDatabase.GetStringFromDataRow(ExpertDataRow, "author_last_name");
            _Paper.PaperID = CDatabase.GetIntFromDataRow(ExpertDataRow, "paper_id");
            _Paper.PaperTitle = CDatabase.GetStringFromDataRow(ExpertDataRow, "paper_title");
            _Paper.CitationCount = CDatabase.GetIntFromDataRow(ExpertDataRow, "citation_count");
            _Paper.CategoryID = CDatabase.GetIntFromDataRow(ExpertDataRow, "category_id");
            _Paper.CategoryName = CDatabase.GetStringFromDataRow(ExpertDataRow, "category_name");
        }

        public int AuthorID
        {
            get { return _Author.AuthorID; }
            set { _Author.AuthorID = value; }
        }

        public string AuthorName
        {
            get { return _Author.AuthorName; }
        }

        public string PaperTitle
        {
            get { return _Paper.PaperTitle; }
            set { _Paper.PaperTitle = value; }
        }
        public int CitationCount
        {
            get { return _Paper.CitationCount; }
            set { _Paper.CitationCount = value; }
        }

        public int CategoryID
        {
            get { return _Paper.CategoryID; }
            set { _Paper.CategoryID = value; }
        }

        public string Category
        {
            get { return _Paper.CategoryName; }
            set { _Paper.CategoryName = value; }
        }
    }
}
