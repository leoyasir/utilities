﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Scrapping;

namespace Scrapping.Classes
{
    public class Publication_Helper
    {
        AuthorsByPublications Author;

        public Publication_Helper()
        {
            Author = null;
        }

        public static List<AuthorsByPublications> CountPublicationsByAuthors
        {
            get
            {
                List<AuthorsByPublications> PublicationList = new List<AuthorsByPublications>();
                try
                {
                    DataRowCollection AuthorsDataRowCollection;
                    CDatabase db = new CDatabase(false);
                    DataSet AuthorsDS;
                    AuthorsDS = db.ExecuteDataSetSP("sp_Number_Of_Publications");
                    if (AuthorsDS != null)
                    {
                        AuthorsDataRowCollection = AuthorsDS.Tables[0].Rows;
                        PublicationList.Clear();
                        foreach (DataRow AuthorDataRow in AuthorsDataRowCollection)
                        {
                            PublicationList.Add(new AuthorsByPublications(AuthorDataRow));
                        }
                    }
                    db.CloseDbConnection();
                }
                catch (Exception dbEx)
                {
                    CMessage.ShowError(dbEx.Message);
                    return null;
                }
                return PublicationList;
            }
        }

        public List<PublicationsOfAuthor> PublicationsOfAuthors
        {
            get
            {
                List<PublicationsOfAuthor> PublicationList = new List<PublicationsOfAuthor>();
                try
                {
                    DataRowCollection AuthorsDataRowCollection;
                    CDatabase db = new CDatabase(false);
                    DataSet AuthorsDS;
                    db.BindInParameter("@author_id", SelectedAuthor.AuthorID, SqlDbType.Int);
                    AuthorsDS = db.ExecuteDataSetSP("sp_Author_Publications");
                    if (AuthorsDS != null)
                    {
                        AuthorsDataRowCollection = AuthorsDS.Tables[0].Rows;
                        PublicationList.Clear();
                        foreach (DataRow AuthorDataRow in AuthorsDataRowCollection)
                        {
                            PublicationList.Add(new PublicationsOfAuthor(AuthorDataRow));
                        }
                    }
                    db.CloseDbConnection();
                }
                catch (Exception dbEx)
                {
                    CMessage.ShowError(dbEx.Message);
                    return null;
                }
                return PublicationList;
            }
        }

        public AuthorsByPublications SelectedAuthor
        {
            get { return Author; }
            set { Author = value; }
        }

    }
}
