﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Scrapping.Classes
{
    public struct STAuthor
    {
        public int AuthorID;
        public string AuthorFName;
        public string AuthorLName;

        public string AuthorName
        {
            get { return AuthorFName + " " + AuthorLName; }
        }
    }
}
