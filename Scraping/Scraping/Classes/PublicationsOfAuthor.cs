﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Scrapping;

namespace Scrapping.Classes
{
    public class PublicationsOfAuthor
    {
        private int _PaperID;
        private STAuthor _Author;
        private string _PaperTitle;

        public PublicationsOfAuthor()
        {
            _Author.AuthorID = _PaperID = -1;
            _Author.AuthorFName = _Author.AuthorLName = _PaperTitle = "";
        }

        public PublicationsOfAuthor(DataRow AuthorDataRow): this()
        {
            _Author.AuthorID = CDatabase.GetIntFromDataRow(AuthorDataRow, "author_id");
            _Author.AuthorFName = CDatabase.GetStringFromDataRow(AuthorDataRow, "author_first_name");
            _Author.AuthorLName = CDatabase.GetStringFromDataRow(AuthorDataRow, "author_last_name");
            _PaperID = CDatabase.GetIntFromDataRow(AuthorDataRow, "paper_id");
            _PaperTitle = CDatabase.GetStringFromDataRow(AuthorDataRow, "paper_title");
        }

        public int AuthorID
        {
            get { return _Author.AuthorID; }
            set { _Author.AuthorID = value; }
        }

        public string AuthorName
        {
            get { return _Author.AuthorName; }
        }

        public int PaperID
        {
            get { return _PaperID; }
            set { _PaperID = value; }
        }

        public string PaperTitle
        {
            get { return _PaperTitle; }
            set { _PaperTitle = value; }
        }
    }
}
