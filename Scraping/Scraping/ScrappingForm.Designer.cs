﻿namespace Scrapping
{
    partial class ScrappingForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.ExpertTabs = new System.Windows.Forms.TabControl();
            this.tabRead = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.gridPatients = new System.Windows.Forms.DataGridView();
            this.titleDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.urlDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.authorsDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.conferenceDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DownloadLink = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.citationsCountDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.paperInfoBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.txtQuery = new System.Windows.Forms.TextBox();
            this.AuthorsByPapers = new System.Windows.Forms.TabPage();
            this.GridPaperCount = new System.Windows.Forms.DataGridView();
            this.authorIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.authorNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.numberOfPublicationsDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.authorsByPublicationsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.tabCitations = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.GridCitations = new System.Windows.Forms.DataGridView();
            this.AuthorID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.authorNameDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CitationCount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.authorsByCitationBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.btnGO = new System.Windows.Forms.Button();
            this.cmbCitCategories = new System.Windows.Forms.ComboBox();
            this.tabCoAuthors = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.gridCoAuthors = new System.Windows.Forms.DataGridView();
            this.authorIDDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.authorNameDataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CoAuthorCount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.authorsByCoAuthorsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.btnGoCoAuth = new System.Windows.Forms.Button();
            this.cmbCoCategory = new System.Windows.Forms.ComboBox();
            this.CategoryExpert = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.GridCategory = new System.Windows.Forms.DataGridView();
            this.authorIDDataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.authorNameDataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.publicationWeightDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.citationWeightDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.coAuthorWeightDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.categoryNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TotalWeight = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.HIndex = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.categoryExpertBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.cmbCategory = new System.Windows.Forms.ComboBox();
            this.btnFindExpert = new System.Windows.Forms.Button();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.listAllPublicationsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ExpertMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.DetailItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportToExcelMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.button2 = new System.Windows.Forms.Button();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.ExpertTabs.SuspendLayout();
            this.tabRead.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridPatients)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.paperInfoBindingSource)).BeginInit();
            this.AuthorsByPapers.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridPaperCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.authorsByPublicationsBindingSource)).BeginInit();
            this.tabCitations.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridCitations)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.authorsByCitationBindingSource)).BeginInit();
            this.tabCoAuthors.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridCoAuthors)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.authorsByCoAuthorsBindingSource)).BeginInit();
            this.CategoryExpert.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridCategory)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.categoryExpertBindingSource)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            this.ExpertMenu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // ExpertTabs
            // 
            this.ExpertTabs.Controls.Add(this.tabRead);
            this.ExpertTabs.Controls.Add(this.AuthorsByPapers);
            this.ExpertTabs.Controls.Add(this.tabCitations);
            this.ExpertTabs.Controls.Add(this.tabCoAuthors);
            this.ExpertTabs.Controls.Add(this.CategoryExpert);
            this.ExpertTabs.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ExpertTabs.Location = new System.Drawing.Point(0, 0);
            this.ExpertTabs.Name = "ExpertTabs";
            this.ExpertTabs.SelectedIndex = 0;
            this.ExpertTabs.Size = new System.Drawing.Size(784, 562);
            this.ExpertTabs.TabIndex = 0;
            this.ExpertTabs.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
            // 
            // tabRead
            // 
            this.tabRead.Controls.Add(this.tableLayoutPanel1);
            this.tabRead.Location = new System.Drawing.Point(4, 22);
            this.tabRead.Name = "tabRead";
            this.tabRead.Padding = new System.Windows.Forms.Padding(3);
            this.tabRead.Size = new System.Drawing.Size(776, 536);
            this.tabRead.TabIndex = 0;
            this.tabRead.Text = "Read And Store in DB";
            this.tabRead.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel1.Controls.Add(this.gridPatients, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.button1, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.txtQuery, 1, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 34F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(770, 530);
            this.tableLayoutPanel1.TabIndex = 4;
            // 
            // gridPatients
            // 
            this.gridPatients.AllowUserToAddRows = false;
            this.gridPatients.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.LightGreen;
            this.gridPatients.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.gridPatients.AutoGenerateColumns = false;
            this.gridPatients.BackgroundColor = System.Drawing.Color.White;
            this.gridPatients.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.gridPatients.ColumnHeadersHeight = 41;
            this.gridPatients.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.gridPatients.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.titleDataGridViewTextBoxColumn,
            this.urlDataGridViewTextBoxColumn,
            this.authorsDataGridViewTextBoxColumn,
            this.conferenceDataGridViewTextBoxColumn,
            this.DownloadLink,
            this.citationsCountDataGridViewTextBoxColumn});
            this.tableLayoutPanel1.SetColumnSpan(this.gridPatients, 3);
            this.gridPatients.DataSource = this.paperInfoBindingSource;
            this.gridPatients.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridPatients.Location = new System.Drawing.Point(3, 37);
            this.gridPatients.MultiSelect = false;
            this.gridPatients.Name = "gridPatients";
            this.gridPatients.ReadOnly = true;
            this.gridPatients.RowHeadersVisible = false;
            this.gridPatients.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gridPatients.Size = new System.Drawing.Size(764, 490);
            this.gridPatients.TabIndex = 15;
            // 
            // titleDataGridViewTextBoxColumn
            // 
            this.titleDataGridViewTextBoxColumn.DataPropertyName = "Title";
            this.titleDataGridViewTextBoxColumn.HeaderText = "Title";
            this.titleDataGridViewTextBoxColumn.Name = "titleDataGridViewTextBoxColumn";
            this.titleDataGridViewTextBoxColumn.ReadOnly = true;
            this.titleDataGridViewTextBoxColumn.Width = 52;
            // 
            // urlDataGridViewTextBoxColumn
            // 
            this.urlDataGridViewTextBoxColumn.DataPropertyName = "Url";
            this.urlDataGridViewTextBoxColumn.HeaderText = "Url";
            this.urlDataGridViewTextBoxColumn.Name = "urlDataGridViewTextBoxColumn";
            this.urlDataGridViewTextBoxColumn.ReadOnly = true;
            this.urlDataGridViewTextBoxColumn.Width = 45;
            // 
            // authorsDataGridViewTextBoxColumn
            // 
            this.authorsDataGridViewTextBoxColumn.DataPropertyName = "Authors";
            this.authorsDataGridViewTextBoxColumn.HeaderText = "Authors";
            this.authorsDataGridViewTextBoxColumn.Name = "authorsDataGridViewTextBoxColumn";
            this.authorsDataGridViewTextBoxColumn.ReadOnly = true;
            this.authorsDataGridViewTextBoxColumn.Width = 68;
            // 
            // conferenceDataGridViewTextBoxColumn
            // 
            this.conferenceDataGridViewTextBoxColumn.DataPropertyName = "Conference";
            this.conferenceDataGridViewTextBoxColumn.HeaderText = "Conference";
            this.conferenceDataGridViewTextBoxColumn.Name = "conferenceDataGridViewTextBoxColumn";
            this.conferenceDataGridViewTextBoxColumn.ReadOnly = true;
            this.conferenceDataGridViewTextBoxColumn.Width = 87;
            // 
            // DownloadLink
            // 
            this.DownloadLink.DataPropertyName = "DownloadLink";
            this.DownloadLink.HeaderText = "Download Link";
            this.DownloadLink.Name = "DownloadLink";
            this.DownloadLink.ReadOnly = true;
            // 
            // citationsCountDataGridViewTextBoxColumn
            // 
            this.citationsCountDataGridViewTextBoxColumn.DataPropertyName = "CitationsCount";
            this.citationsCountDataGridViewTextBoxColumn.HeaderText = "CitationsCount";
            this.citationsCountDataGridViewTextBoxColumn.Name = "citationsCountDataGridViewTextBoxColumn";
            this.citationsCountDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // paperInfoBindingSource
            // 
            this.paperInfoBindingSource.DataSource = typeof(Scrapping.Classes.ScholarPaperInfo);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(3, 3);
            this.label1.Margin = new System.Windows.Forms.Padding(3);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(144, 28);
            this.label1.TabIndex = 1;
            this.label1.Text = "Enter your query";
            // 
            // button1
            // 
            this.button1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button1.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(673, 3);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(94, 28);
            this.button1.TabIndex = 0;
            this.button1.Text = "Go";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // txtQuery
            // 
            this.txtQuery.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtQuery.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtQuery.Location = new System.Drawing.Point(153, 3);
            this.txtQuery.Name = "txtQuery";
            this.txtQuery.Size = new System.Drawing.Size(514, 27);
            this.txtQuery.TabIndex = 2;
            // 
            // AuthorsByPapers
            // 
            this.AuthorsByPapers.Controls.Add(this.GridPaperCount);
            this.AuthorsByPapers.Location = new System.Drawing.Point(4, 22);
            this.AuthorsByPapers.Name = "AuthorsByPapers";
            this.AuthorsByPapers.Padding = new System.Windows.Forms.Padding(3);
            this.AuthorsByPapers.Size = new System.Drawing.Size(776, 536);
            this.AuthorsByPapers.TabIndex = 1;
            this.AuthorsByPapers.Text = "Authors by Number of Papers";
            this.AuthorsByPapers.UseVisualStyleBackColor = true;
            this.AuthorsByPapers.Click += new System.EventHandler(this.AuthorsByPapers_Click);
            // 
            // GridPaperCount
            // 
            this.GridPaperCount.AllowUserToAddRows = false;
            this.GridPaperCount.AllowUserToDeleteRows = false;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.LightGreen;
            this.GridPaperCount.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle2;
            this.GridPaperCount.AutoGenerateColumns = false;
            this.GridPaperCount.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.GridPaperCount.BackgroundColor = System.Drawing.Color.White;
            this.GridPaperCount.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.GridPaperCount.ColumnHeadersHeight = 41;
            this.GridPaperCount.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.GridPaperCount.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.authorIDDataGridViewTextBoxColumn,
            this.authorNameDataGridViewTextBoxColumn,
            this.numberOfPublicationsDataGridViewTextBoxColumn});
            this.GridPaperCount.DataSource = this.authorsByPublicationsBindingSource;
            this.GridPaperCount.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GridPaperCount.Location = new System.Drawing.Point(3, 3);
            this.GridPaperCount.MultiSelect = false;
            this.GridPaperCount.Name = "GridPaperCount";
            this.GridPaperCount.ReadOnly = true;
            this.GridPaperCount.RowHeadersVisible = false;
            this.GridPaperCount.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.GridPaperCount.Size = new System.Drawing.Size(770, 530);
            this.GridPaperCount.TabIndex = 16;
            this.GridPaperCount.CellMouseUp += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.GridPaperCount_CellMouseUp);
            this.GridPaperCount.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.GridPaperCount_CellDoubleClick);
            // 
            // authorIDDataGridViewTextBoxColumn
            // 
            this.authorIDDataGridViewTextBoxColumn.DataPropertyName = "AuthorID";
            this.authorIDDataGridViewTextBoxColumn.HeaderText = "Author ID";
            this.authorIDDataGridViewTextBoxColumn.Name = "authorIDDataGridViewTextBoxColumn";
            this.authorIDDataGridViewTextBoxColumn.ReadOnly = true;
            this.authorIDDataGridViewTextBoxColumn.Width = 71;
            // 
            // authorNameDataGridViewTextBoxColumn
            // 
            this.authorNameDataGridViewTextBoxColumn.DataPropertyName = "AuthorName";
            this.authorNameDataGridViewTextBoxColumn.HeaderText = "Author Name";
            this.authorNameDataGridViewTextBoxColumn.Name = "authorNameDataGridViewTextBoxColumn";
            this.authorNameDataGridViewTextBoxColumn.ReadOnly = true;
            this.authorNameDataGridViewTextBoxColumn.Width = 87;
            // 
            // numberOfPublicationsDataGridViewTextBoxColumn
            // 
            this.numberOfPublicationsDataGridViewTextBoxColumn.DataPropertyName = "NumberOfPublications";
            this.numberOfPublicationsDataGridViewTextBoxColumn.HeaderText = "Number Of Publications";
            this.numberOfPublicationsDataGridViewTextBoxColumn.Name = "numberOfPublicationsDataGridViewTextBoxColumn";
            this.numberOfPublicationsDataGridViewTextBoxColumn.ReadOnly = true;
            this.numberOfPublicationsDataGridViewTextBoxColumn.Width = 131;
            // 
            // authorsByPublicationsBindingSource
            // 
            this.authorsByPublicationsBindingSource.DataSource = typeof(Scrapping.Classes.AuthorsByPublications);
            // 
            // tabCitations
            // 
            this.tabCitations.Controls.Add(this.tableLayoutPanel3);
            this.tabCitations.Location = new System.Drawing.Point(4, 22);
            this.tabCitations.Name = "tabCitations";
            this.tabCitations.Padding = new System.Windows.Forms.Padding(3);
            this.tabCitations.Size = new System.Drawing.Size(776, 536);
            this.tabCitations.TabIndex = 4;
            this.tabCitations.Text = "Authors by Citations";
            this.tabCitations.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 2;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 120F));
            this.tableLayoutPanel3.Controls.Add(this.GridCitations, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.btnGO, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.cmbCitCategories, 0, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 2;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 33F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(770, 530);
            this.tableLayoutPanel3.TabIndex = 18;
            // 
            // GridCitations
            // 
            this.GridCitations.AllowUserToAddRows = false;
            this.GridCitations.AllowUserToDeleteRows = false;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.LightGreen;
            this.GridCitations.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle3;
            this.GridCitations.AutoGenerateColumns = false;
            this.GridCitations.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.GridCitations.BackgroundColor = System.Drawing.Color.White;
            this.GridCitations.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.GridCitations.ColumnHeadersHeight = 41;
            this.GridCitations.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.GridCitations.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.AuthorID,
            this.authorNameDataGridViewTextBoxColumn1,
            this.CitationCount});
            this.tableLayoutPanel3.SetColumnSpan(this.GridCitations, 2);
            this.GridCitations.DataSource = this.authorsByCitationBindingSource;
            this.GridCitations.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GridCitations.Location = new System.Drawing.Point(3, 36);
            this.GridCitations.MultiSelect = false;
            this.GridCitations.Name = "GridCitations";
            this.GridCitations.ReadOnly = true;
            this.GridCitations.RowHeadersVisible = false;
            this.GridCitations.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.GridCitations.Size = new System.Drawing.Size(764, 491);
            this.GridCitations.TabIndex = 17;
            // 
            // AuthorID
            // 
            this.AuthorID.DataPropertyName = "AuthorID";
            this.AuthorID.HeaderText = "Author ID";
            this.AuthorID.Name = "AuthorID";
            this.AuthorID.ReadOnly = true;
            this.AuthorID.Width = 71;
            // 
            // authorNameDataGridViewTextBoxColumn1
            // 
            this.authorNameDataGridViewTextBoxColumn1.DataPropertyName = "AuthorName";
            this.authorNameDataGridViewTextBoxColumn1.HeaderText = "Author Name";
            this.authorNameDataGridViewTextBoxColumn1.Name = "authorNameDataGridViewTextBoxColumn1";
            this.authorNameDataGridViewTextBoxColumn1.ReadOnly = true;
            this.authorNameDataGridViewTextBoxColumn1.Width = 87;
            // 
            // CitationCount
            // 
            this.CitationCount.DataPropertyName = "CitationCount";
            this.CitationCount.HeaderText = "Citation Count";
            this.CitationCount.Name = "CitationCount";
            this.CitationCount.ReadOnly = true;
            this.CitationCount.Width = 90;
            // 
            // authorsByCitationBindingSource
            // 
            this.authorsByCitationBindingSource.DataSource = typeof(Scrapping.Classes.AuthorsByCitation);
            // 
            // btnGO
            // 
            this.btnGO.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnGO.Location = new System.Drawing.Point(653, 3);
            this.btnGO.Name = "btnGO";
            this.btnGO.Size = new System.Drawing.Size(114, 27);
            this.btnGO.TabIndex = 18;
            this.btnGO.Text = "Go";
            this.btnGO.UseVisualStyleBackColor = true;
            this.btnGO.Click += new System.EventHandler(this.btnGO_Click);
            // 
            // cmbCitCategories
            // 
            this.cmbCitCategories.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmbCitCategories.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbCitCategories.Font = new System.Drawing.Font("Calibri", 12F);
            this.cmbCitCategories.FormattingEnabled = true;
            this.cmbCitCategories.Location = new System.Drawing.Point(3, 3);
            this.cmbCitCategories.Name = "cmbCitCategories";
            this.cmbCitCategories.Size = new System.Drawing.Size(644, 27);
            this.cmbCitCategories.TabIndex = 19;
            // 
            // tabCoAuthors
            // 
            this.tabCoAuthors.Controls.Add(this.tableLayoutPanel4);
            this.tabCoAuthors.Location = new System.Drawing.Point(4, 22);
            this.tabCoAuthors.Name = "tabCoAuthors";
            this.tabCoAuthors.Size = new System.Drawing.Size(776, 536);
            this.tabCoAuthors.TabIndex = 5;
            this.tabCoAuthors.Text = "Co-Author Network";
            this.tabCoAuthors.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 2;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 120F));
            this.tableLayoutPanel4.Controls.Add(this.gridCoAuthors, 0, 1);
            this.tableLayoutPanel4.Controls.Add(this.btnGoCoAuth, 1, 0);
            this.tableLayoutPanel4.Controls.Add(this.cmbCoCategory, 0, 0);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 2;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 33F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(776, 536);
            this.tableLayoutPanel4.TabIndex = 19;
            // 
            // gridCoAuthors
            // 
            this.gridCoAuthors.AllowUserToAddRows = false;
            this.gridCoAuthors.AllowUserToDeleteRows = false;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.LightGreen;
            this.gridCoAuthors.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle4;
            this.gridCoAuthors.AutoGenerateColumns = false;
            this.gridCoAuthors.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.gridCoAuthors.BackgroundColor = System.Drawing.Color.White;
            this.gridCoAuthors.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.gridCoAuthors.ColumnHeadersHeight = 41;
            this.gridCoAuthors.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.gridCoAuthors.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.authorIDDataGridViewTextBoxColumn1,
            this.authorNameDataGridViewTextBoxColumn3,
            this.CoAuthorCount});
            this.tableLayoutPanel4.SetColumnSpan(this.gridCoAuthors, 2);
            this.gridCoAuthors.DataSource = this.authorsByCoAuthorsBindingSource;
            this.gridCoAuthors.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridCoAuthors.Location = new System.Drawing.Point(3, 36);
            this.gridCoAuthors.MultiSelect = false;
            this.gridCoAuthors.Name = "gridCoAuthors";
            this.gridCoAuthors.ReadOnly = true;
            this.gridCoAuthors.RowHeadersVisible = false;
            this.gridCoAuthors.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gridCoAuthors.Size = new System.Drawing.Size(770, 497);
            this.gridCoAuthors.TabIndex = 17;
            this.gridCoAuthors.CellMouseUp += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.gridCoAuthors_CellMouseUp);
            this.gridCoAuthors.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.gridCoAuthors_CellDoubleClick);
            // 
            // authorIDDataGridViewTextBoxColumn1
            // 
            this.authorIDDataGridViewTextBoxColumn1.DataPropertyName = "AuthorID";
            this.authorIDDataGridViewTextBoxColumn1.HeaderText = "Author ID";
            this.authorIDDataGridViewTextBoxColumn1.Name = "authorIDDataGridViewTextBoxColumn1";
            this.authorIDDataGridViewTextBoxColumn1.ReadOnly = true;
            this.authorIDDataGridViewTextBoxColumn1.Width = 71;
            // 
            // authorNameDataGridViewTextBoxColumn3
            // 
            this.authorNameDataGridViewTextBoxColumn3.DataPropertyName = "AuthorName";
            this.authorNameDataGridViewTextBoxColumn3.HeaderText = "Author Name";
            this.authorNameDataGridViewTextBoxColumn3.Name = "authorNameDataGridViewTextBoxColumn3";
            this.authorNameDataGridViewTextBoxColumn3.ReadOnly = true;
            this.authorNameDataGridViewTextBoxColumn3.Width = 87;
            // 
            // CoAuthorCount
            // 
            this.CoAuthorCount.DataPropertyName = "CoAuthorCount";
            this.CoAuthorCount.HeaderText = "Co-Author Count";
            this.CoAuthorCount.Name = "CoAuthorCount";
            this.CoAuthorCount.ReadOnly = true;
            this.CoAuthorCount.Width = 101;
            // 
            // authorsByCoAuthorsBindingSource
            // 
            this.authorsByCoAuthorsBindingSource.DataSource = typeof(Scrapping.Classes.AuthorsByCoAuthors);
            // 
            // btnGoCoAuth
            // 
            this.btnGoCoAuth.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnGoCoAuth.Location = new System.Drawing.Point(659, 3);
            this.btnGoCoAuth.Name = "btnGoCoAuth";
            this.btnGoCoAuth.Size = new System.Drawing.Size(114, 27);
            this.btnGoCoAuth.TabIndex = 18;
            this.btnGoCoAuth.Text = "Go";
            this.btnGoCoAuth.UseVisualStyleBackColor = true;
            this.btnGoCoAuth.Click += new System.EventHandler(this.btnGoCoAuth_Click);
            // 
            // cmbCoCategory
            // 
            this.cmbCoCategory.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmbCoCategory.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbCoCategory.Font = new System.Drawing.Font("Calibri", 12F);
            this.cmbCoCategory.FormattingEnabled = true;
            this.cmbCoCategory.Location = new System.Drawing.Point(3, 3);
            this.cmbCoCategory.Name = "cmbCoCategory";
            this.cmbCoCategory.Size = new System.Drawing.Size(650, 27);
            this.cmbCoCategory.TabIndex = 19;
            // 
            // CategoryExpert
            // 
            this.CategoryExpert.Controls.Add(this.tableLayoutPanel2);
            this.CategoryExpert.Location = new System.Drawing.Point(4, 22);
            this.CategoryExpert.Name = "CategoryExpert";
            this.CategoryExpert.Padding = new System.Windows.Forms.Padding(3);
            this.CategoryExpert.Size = new System.Drawing.Size(776, 536);
            this.CategoryExpert.TabIndex = 3;
            this.CategoryExpert.Text = "Expert of Category";
            this.CategoryExpert.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 120F));
            this.tableLayoutPanel2.Controls.Add(this.GridCategory, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.cmbCategory, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.btnFindExpert, 1, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 33F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(770, 530);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // GridCategory
            // 
            this.GridCategory.AllowUserToAddRows = false;
            this.GridCategory.AllowUserToDeleteRows = false;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.LightGreen;
            this.GridCategory.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle5;
            this.GridCategory.AutoGenerateColumns = false;
            this.GridCategory.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.GridCategory.BackgroundColor = System.Drawing.Color.White;
            this.GridCategory.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.GridCategory.ColumnHeadersHeight = 41;
            this.GridCategory.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.GridCategory.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.authorIDDataGridViewTextBoxColumn2,
            this.authorNameDataGridViewTextBoxColumn2,
            this.publicationWeightDataGridViewTextBoxColumn,
            this.citationWeightDataGridViewTextBoxColumn,
            this.coAuthorWeightDataGridViewTextBoxColumn,
            this.categoryNameDataGridViewTextBoxColumn,
            this.TotalWeight,
            this.HIndex});
            this.tableLayoutPanel2.SetColumnSpan(this.GridCategory, 2);
            this.GridCategory.DataSource = this.categoryExpertBindingSource;
            this.GridCategory.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GridCategory.Location = new System.Drawing.Point(3, 36);
            this.GridCategory.MultiSelect = false;
            this.GridCategory.Name = "GridCategory";
            this.GridCategory.ReadOnly = true;
            this.GridCategory.RowHeadersVisible = false;
            this.GridCategory.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.GridCategory.Size = new System.Drawing.Size(764, 491);
            this.GridCategory.TabIndex = 18;
            this.GridCategory.CellMouseUp += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.GridCategory_CellMouseUp);
            this.GridCategory.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.GridCategory_CellDoubleClick);
            // 
            // authorIDDataGridViewTextBoxColumn2
            // 
            this.authorIDDataGridViewTextBoxColumn2.DataPropertyName = "AuthorID";
            this.authorIDDataGridViewTextBoxColumn2.HeaderText = "Author ID";
            this.authorIDDataGridViewTextBoxColumn2.Name = "authorIDDataGridViewTextBoxColumn2";
            this.authorIDDataGridViewTextBoxColumn2.ReadOnly = true;
            this.authorIDDataGridViewTextBoxColumn2.Width = 71;
            // 
            // authorNameDataGridViewTextBoxColumn2
            // 
            this.authorNameDataGridViewTextBoxColumn2.DataPropertyName = "AuthorName";
            this.authorNameDataGridViewTextBoxColumn2.HeaderText = "Author Name";
            this.authorNameDataGridViewTextBoxColumn2.Name = "authorNameDataGridViewTextBoxColumn2";
            this.authorNameDataGridViewTextBoxColumn2.ReadOnly = true;
            this.authorNameDataGridViewTextBoxColumn2.Width = 87;
            // 
            // publicationWeightDataGridViewTextBoxColumn
            // 
            this.publicationWeightDataGridViewTextBoxColumn.DataPropertyName = "PublicationWeight";
            this.publicationWeightDataGridViewTextBoxColumn.HeaderText = "Publication Weight";
            this.publicationWeightDataGridViewTextBoxColumn.Name = "publicationWeightDataGridViewTextBoxColumn";
            this.publicationWeightDataGridViewTextBoxColumn.ReadOnly = true;
            this.publicationWeightDataGridViewTextBoxColumn.Width = 111;
            // 
            // citationWeightDataGridViewTextBoxColumn
            // 
            this.citationWeightDataGridViewTextBoxColumn.DataPropertyName = "CitationWeight";
            this.citationWeightDataGridViewTextBoxColumn.HeaderText = "Citation Weight";
            this.citationWeightDataGridViewTextBoxColumn.Name = "citationWeightDataGridViewTextBoxColumn";
            this.citationWeightDataGridViewTextBoxColumn.ReadOnly = true;
            this.citationWeightDataGridViewTextBoxColumn.Width = 96;
            // 
            // coAuthorWeightDataGridViewTextBoxColumn
            // 
            this.coAuthorWeightDataGridViewTextBoxColumn.DataPropertyName = "CoAuthorWeight";
            this.coAuthorWeightDataGridViewTextBoxColumn.HeaderText = "CoAuthor Weight";
            this.coAuthorWeightDataGridViewTextBoxColumn.Name = "coAuthorWeightDataGridViewTextBoxColumn";
            this.coAuthorWeightDataGridViewTextBoxColumn.ReadOnly = true;
            this.coAuthorWeightDataGridViewTextBoxColumn.Width = 104;
            // 
            // categoryNameDataGridViewTextBoxColumn
            // 
            this.categoryNameDataGridViewTextBoxColumn.DataPropertyName = "CategoryName";
            this.categoryNameDataGridViewTextBoxColumn.HeaderText = "Category";
            this.categoryNameDataGridViewTextBoxColumn.Name = "categoryNameDataGridViewTextBoxColumn";
            this.categoryNameDataGridViewTextBoxColumn.ReadOnly = true;
            this.categoryNameDataGridViewTextBoxColumn.Width = 74;
            // 
            // TotalWeight
            // 
            this.TotalWeight.DataPropertyName = "TotalWeight";
            this.TotalWeight.HeaderText = "Total Weight";
            this.TotalWeight.Name = "TotalWeight";
            this.TotalWeight.ReadOnly = true;
            this.TotalWeight.Width = 86;
            // 
            // HIndex
            // 
            this.HIndex.DataPropertyName = "HIndex";
            this.HIndex.HeaderText = "HIndex";
            this.HIndex.Name = "HIndex";
            this.HIndex.ReadOnly = true;
            this.HIndex.Width = 66;
            // 
            // categoryExpertBindingSource
            // 
            this.categoryExpertBindingSource.DataSource = typeof(Scrapping.Classes.Category_Expert);
            // 
            // cmbCategory
            // 
            this.cmbCategory.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmbCategory.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbCategory.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbCategory.FormattingEnabled = true;
            this.cmbCategory.Location = new System.Drawing.Point(3, 3);
            this.cmbCategory.Name = "cmbCategory";
            this.cmbCategory.Size = new System.Drawing.Size(644, 27);
            this.cmbCategory.TabIndex = 0;
            // 
            // btnFindExpert
            // 
            this.btnFindExpert.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnFindExpert.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFindExpert.Location = new System.Drawing.Point(653, 3);
            this.btnFindExpert.Name = "btnFindExpert";
            this.btnFindExpert.Size = new System.Drawing.Size(114, 27);
            this.btnFindExpert.TabIndex = 1;
            this.btnFindExpert.Text = "Find Expert";
            this.btnFindExpert.UseVisualStyleBackColor = true;
            this.btnFindExpert.Click += new System.EventHandler(this.btnFindExpert_Click);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.listAllPublicationsToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(178, 26);
            // 
            // listAllPublicationsToolStripMenuItem
            // 
            this.listAllPublicationsToolStripMenuItem.Name = "listAllPublicationsToolStripMenuItem";
            this.listAllPublicationsToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.listAllPublicationsToolStripMenuItem.Text = "List All Publications";
            this.listAllPublicationsToolStripMenuItem.Click += new System.EventHandler(this.listAllPublicationsToolStripMenuItem_Click);
            // 
            // ExpertMenu
            // 
            this.ExpertMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.DetailItem,
            this.exportToExcelMenuItem});
            this.ExpertMenu.Name = "ExpertMenu";
            this.ExpertMenu.Size = new System.Drawing.Size(151, 48);
            // 
            // DetailItem
            // 
            this.DetailItem.Name = "DetailItem";
            this.DetailItem.Size = new System.Drawing.Size(150, 22);
            this.DetailItem.Text = "View Detail";
            this.DetailItem.Click += new System.EventHandler(this.DetailItem_Click);
            // 
            // exportToExcelMenuItem
            // 
            this.exportToExcelMenuItem.Name = "exportToExcelMenuItem";
            this.exportToExcelMenuItem.Size = new System.Drawing.Size(150, 22);
            this.exportToExcelMenuItem.Text = "Export to Excel";
            this.exportToExcelMenuItem.Click += new System.EventHandler(this.exportToExcelMenuItem_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.LightGreen;
            this.dataGridView1.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle6;
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dataGridView1.BackgroundColor = System.Drawing.Color.White;
            this.dataGridView1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dataGridView1.ColumnHeadersHeight = 41;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3});
            this.dataGridView1.DataSource = this.authorsByCitationBindingSource;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(3, 36);
            this.dataGridView1.MultiSelect = false;
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(764, 491);
            this.dataGridView1.TabIndex = 17;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "AuthorID";
            this.dataGridViewTextBoxColumn1.HeaderText = "Author ID";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Width = 71;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "AuthorName";
            this.dataGridViewTextBoxColumn2.HeaderText = "Author Name";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.Width = 87;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "CitationCount";
            this.dataGridViewTextBoxColumn3.HeaderText = "Citation Count";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.Width = 90;
            // 
            // button2
            // 
            this.button2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button2.Location = new System.Drawing.Point(653, 3);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(114, 27);
            this.button2.TabIndex = 18;
            this.button2.Text = "Go";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // comboBox1
            // 
            this.comboBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.Font = new System.Drawing.Font("Calibri", 12F);
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(3, 3);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(644, 27);
            this.comboBox1.TabIndex = 19;
            // 
            // ScrappingForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 562);
            this.Controls.Add(this.ExpertTabs);
            this.Name = "ScrappingForm";
            this.Text = "Expert Finder";
            this.ExpertTabs.ResumeLayout(false);
            this.tabRead.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridPatients)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.paperInfoBindingSource)).EndInit();
            this.AuthorsByPapers.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GridPaperCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.authorsByPublicationsBindingSource)).EndInit();
            this.tabCitations.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GridCitations)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.authorsByCitationBindingSource)).EndInit();
            this.tabCoAuthors.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridCoAuthors)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.authorsByCoAuthorsBindingSource)).EndInit();
            this.CategoryExpert.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GridCategory)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.categoryExpertBindingSource)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            this.ExpertMenu.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl ExpertTabs;
        private System.Windows.Forms.TabPage tabRead;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.DataGridView gridPatients;
        private System.Windows.Forms.DataGridViewTextBoxColumn titleDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn urlDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn authorsDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn conferenceDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn DownloadLink;
        private System.Windows.Forms.DataGridViewTextBoxColumn citationsCountDataGridViewTextBoxColumn;
        private System.Windows.Forms.BindingSource paperInfoBindingSource;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox txtQuery;
        private System.Windows.Forms.TabPage AuthorsByPapers;
        private System.Windows.Forms.DataGridView GridPaperCount;
        private System.Windows.Forms.BindingSource authorsByPublicationsBindingSource;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem listAllPublicationsToolStripMenuItem;
        private System.Windows.Forms.TabPage CategoryExpert;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.ComboBox cmbCategory;
        private System.Windows.Forms.Button btnFindExpert;
        private System.Windows.Forms.DataGridView GridCategory;
        private System.Windows.Forms.BindingSource categoryExpertBindingSource;
        private System.Windows.Forms.ContextMenuStrip ExpertMenu;
        private System.Windows.Forms.ToolStripMenuItem DetailItem;
        private System.Windows.Forms.TabPage tabCitations;
        private System.Windows.Forms.DataGridView GridCitations;
        private System.Windows.Forms.BindingSource authorsByCitationBindingSource;
        private System.Windows.Forms.DataGridViewTextBoxColumn AuthorID;
        private System.Windows.Forms.DataGridViewTextBoxColumn authorNameDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn CitationCount;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Button btnGO;
        private System.Windows.Forms.ComboBox cmbCitCategories;
        private System.Windows.Forms.TabPage tabCoAuthors;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.DataGridView gridCoAuthors;
        private System.Windows.Forms.Button btnGoCoAuth;
        private System.Windows.Forms.ComboBox cmbCoCategory;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.BindingSource authorsByCoAuthorsBindingSource;
        private System.Windows.Forms.DataGridViewTextBoxColumn authorIDDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn authorNameDataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn CoAuthorCount;
        private System.Windows.Forms.DataGridViewTextBoxColumn authorIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn authorNameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn numberOfPublicationsDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn authorIDDataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn authorNameDataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn publicationWeightDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn citationWeightDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn coAuthorWeightDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn categoryNameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn TotalWeight;
        private System.Windows.Forms.DataGridViewTextBoxColumn HIndex;
        private System.Windows.Forms.ToolStripMenuItem exportToExcelMenuItem;

    }
}

