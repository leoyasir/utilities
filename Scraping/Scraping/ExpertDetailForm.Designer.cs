﻿namespace Scrapping
{
    partial class ExpertDetailForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.GridCategory = new System.Windows.Forms.DataGridView();
            this.authorIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.authorNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.paperTitleDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.citationCountDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.categoryDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.expertDetailBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridCategory)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.expertDetailBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.GridCategory, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(483, 374);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // GridCategory
            // 
            this.GridCategory.AllowUserToAddRows = false;
            this.GridCategory.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.LightGreen;
            this.GridCategory.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.GridCategory.AutoGenerateColumns = false;
            this.GridCategory.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.GridCategory.BackgroundColor = System.Drawing.Color.White;
            this.GridCategory.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.GridCategory.ColumnHeadersHeight = 41;
            this.GridCategory.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.GridCategory.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.authorIDDataGridViewTextBoxColumn,
            this.authorNameDataGridViewTextBoxColumn,
            this.paperTitleDataGridViewTextBoxColumn,
            this.citationCountDataGridViewTextBoxColumn,
            this.categoryDataGridViewTextBoxColumn});
            this.GridCategory.DataSource = this.expertDetailBindingSource;
            this.GridCategory.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GridCategory.Location = new System.Drawing.Point(3, 3);
            this.GridCategory.MultiSelect = false;
            this.GridCategory.Name = "GridCategory";
            this.GridCategory.ReadOnly = true;
            this.GridCategory.RowHeadersVisible = false;
            this.GridCategory.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.GridCategory.Size = new System.Drawing.Size(477, 368);
            this.GridCategory.TabIndex = 19;
            // 
            // authorIDDataGridViewTextBoxColumn
            // 
            this.authorIDDataGridViewTextBoxColumn.DataPropertyName = "AuthorID";
            this.authorIDDataGridViewTextBoxColumn.HeaderText = "Author ID";
            this.authorIDDataGridViewTextBoxColumn.Name = "authorIDDataGridViewTextBoxColumn";
            this.authorIDDataGridViewTextBoxColumn.ReadOnly = true;
            this.authorIDDataGridViewTextBoxColumn.Width = 71;
            // 
            // authorNameDataGridViewTextBoxColumn
            // 
            this.authorNameDataGridViewTextBoxColumn.DataPropertyName = "AuthorName";
            this.authorNameDataGridViewTextBoxColumn.HeaderText = "Author Name";
            this.authorNameDataGridViewTextBoxColumn.Name = "authorNameDataGridViewTextBoxColumn";
            this.authorNameDataGridViewTextBoxColumn.ReadOnly = true;
            this.authorNameDataGridViewTextBoxColumn.Width = 87;
            // 
            // paperTitleDataGridViewTextBoxColumn
            // 
            this.paperTitleDataGridViewTextBoxColumn.DataPropertyName = "PaperTitle";
            this.paperTitleDataGridViewTextBoxColumn.HeaderText = "Paper Title";
            this.paperTitleDataGridViewTextBoxColumn.Name = "paperTitleDataGridViewTextBoxColumn";
            this.paperTitleDataGridViewTextBoxColumn.ReadOnly = true;
            this.paperTitleDataGridViewTextBoxColumn.Width = 77;
            // 
            // citationCountDataGridViewTextBoxColumn
            // 
            this.citationCountDataGridViewTextBoxColumn.DataPropertyName = "CitationCount";
            this.citationCountDataGridViewTextBoxColumn.HeaderText = "Citation Count";
            this.citationCountDataGridViewTextBoxColumn.Name = "citationCountDataGridViewTextBoxColumn";
            this.citationCountDataGridViewTextBoxColumn.ReadOnly = true;
            this.citationCountDataGridViewTextBoxColumn.Width = 90;
            // 
            // categoryDataGridViewTextBoxColumn
            // 
            this.categoryDataGridViewTextBoxColumn.DataPropertyName = "Category";
            this.categoryDataGridViewTextBoxColumn.HeaderText = "Category";
            this.categoryDataGridViewTextBoxColumn.Name = "categoryDataGridViewTextBoxColumn";
            this.categoryDataGridViewTextBoxColumn.ReadOnly = true;
            this.categoryDataGridViewTextBoxColumn.Width = 74;
            // 
            // expertDetailBindingSource
            // 
            this.expertDetailBindingSource.DataSource = typeof(Scrapping.Classes.ExpertDetail);
            // 
            // ExpertDetailForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(483, 374);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "ExpertDetailForm";
            this.Text = "Expert Detail";
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GridCategory)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.expertDetailBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.DataGridView GridCategory;
        private System.Windows.Forms.DataGridViewTextBoxColumn authorIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn authorNameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn paperTitleDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn citationCountDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn categoryDataGridViewTextBoxColumn;
        private System.Windows.Forms.BindingSource expertDetailBindingSource;


    }
}