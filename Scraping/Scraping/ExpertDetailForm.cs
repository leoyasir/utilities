﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Scrapping.Classes;

namespace Scrapping
{
    public partial class ExpertDetailForm : Form
    {
        private int _AuthorID;
        private int _CategoryID;

        public ExpertDetailForm()
        {
            InitializeComponent();
            _AuthorID = -1;
            _CategoryID = -1;
        }

        public int AuthorID
        {
            get { return _AuthorID; }
            set { _AuthorID = value; }
        }

        public int CategoryID
        {
            get { return _CategoryID; }
            set { _CategoryID = value; }
        }

        public List<ExpertDetail> ExpertDataSource
        {
            set { GridCategory.DataSource = value; }
        }
    }
}
