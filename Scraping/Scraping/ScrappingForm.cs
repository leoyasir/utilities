﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
using HtmlAgilityPack;
using System.IO;
using Scrapping.Classes;
using Excel = Microsoft.Office.Interop.Excel;
using System.Threading;

namespace Scrapping
{
    public partial class ScrappingForm : Form
    {
        List<ScholarPaperInfo> PaperList;
        List<string> StopWordsList;
        int selectedRow = -1;
        int NumOfRec;
        public ScrappingForm()
        {
            InitializeComponent();
            ReadStopWords();
            NumOfRec = 100;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            // Scrape links

            //WebClient w = new WebClient();
            //string s = w.DownloadString("http://scholar.google.com.pk/scholar?q=bio+informatics&num=20&hl=en&btnG=Search&as_sdt=1%2C5&as_sdtp=on");
            if (txtQuery.Text.Trim() == "")
            {
                MessageBox.Show("Enter title to be searched in google scholar");
                return;
            }
            PaperList = null;
            PaperList = new List<ScholarPaperInfo>();
            if (CMessage.Mode == "Offline")
            {
                string[] filePaths = Directory.GetFiles(CMessage.Path, "*.htm", SearchOption.AllDirectories);
                foreach (string file in filePaths)
                {
                    string html = ReadFromFile(file);
                    HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
                    doc.LoadHtml(html);

                    foreach (HtmlNode link in doc.DocumentNode.SelectNodes("//h3[@class='gs_rt']"))
                    {
                        ScholarPaperInfo paper = getPaperInfo(link);
                        if (paper == null)
                            continue;

                        paper.Category = Directory.GetParent(file).Name;
                        paper.Save();
                        
                        PaperList.Add(paper);
                    }
                }
            }
            else
            {
                //Genetics and Population Analysis$Systems Biology$Data and Text Mining Bioinformatics$Bioimaging$Biological databases and ontologies$Computational biology$Proteomics$Transcriptomics$Population genomics$RNA Sequence analysis$Neuroinformatics
                string[] splitChar = {"$"};
                string[] categories = txtQuery.Text.Trim().Split(splitChar, StringSplitOptions.None);
                foreach (string category in categories)
                {
                    string query = category.Trim().Replace(' ', '+');
                    for (int i = 0; i < 10; i++)
                    {
                        HtmlAgilityPack.HtmlWeb web = new HtmlAgilityPack.HtmlWeb();
                        HtmlAgilityPack.HtmlDocument doc = web.Load("http://scholar.google.com.pk/scholar?start=" + (i * 100) + "&q=" + query + "&num=" + NumOfRecPerPage + "&hl=en&btnG=Search&as_sdt=1%2C5&as_sdtp=on");
                        foreach (HtmlNode link in doc.DocumentNode.SelectNodes("//h3[@class='gs_rt']"))
                        {
                            ScholarPaperInfo paper = getPaperInfo(link);
                            if (paper == null)
                                continue;
                            paper.Category = category;//txtQuery.Text.Trim();
                            paper.Save();
                            PaperList.Add(paper);
                        }
                    }
                }
            }
            gridPatients.DataSource = PaperList;
        }

        public ScholarPaperInfo getPaperInfo(HtmlNode link)
        {
            ScholarPaperInfo paper = new ScholarPaperInfo();
            HtmlNode current = link;

            //Get Paper Title
            paper.Title = link.InnerText;
            if (paper.Title.Contains("[CITATION]") || paper.Title.Contains("[BOOK]") || paper.Title == "")
            {
                return null;
            }
            else if (paper.Title.Contains("[HTML]"))
            {
                paper.Title = paper.Title.Substring(6).Trim();
            }
            else if (paper.Title.Contains("[PDF]"))
            {
                paper.Title = paper.Title.Substring(5).Trim();
            }

            paper.Title = RemoveNoise(paper.Title);

            //Get paper URL
            paper.Url = link.SelectSingleNode(".//a").Attributes["href"].Value.ToString();
            paper.Url = RemoveNoise(paper.Url).Trim();
            //Get authors and conference information
            current = link.NextSibling;
            if (current.InnerText.Contains("[PDF]") || current.InnerText.Contains("[HTML]"))
            {
                paper.DownloadLink = current.SelectSingleNode(".//a").Attributes["href"].Value.ToString();
                current = current.NextSibling;
            }

            //Find Authors
            string authorsandconference = current.InnerText;
            string[] array = { " - " };
            string[] temp = authorsandconference.Split(array, System.StringSplitOptions.None);
            //paper.Authors = temp[0];
            paper.Authors = RemoveNoise(temp[0]).Trim();

            //Find Conference Information
            int counter = 1;
            string conference = "";
            while (counter != temp.Length)
            {
                conference += temp[counter] + " ";
                counter++;
            }
            paper.Conference = RemoveNoise(conference).Trim();

            //Get citation information
            while (current != null && current.Attributes.Where<HtmlAttribute>(attr => attr.Name == "class" && attr.Value == "gs_fl").Count<HtmlAttribute>() <= 0)
            {
                current = current.NextSibling;
            }
            if (current != null)
            {
                string citation = current.SelectSingleNode(".//a").InnerText;
                if (citation.Contains("Cited by"))
                {
                    paper.CitationsCount = int.Parse(citation.Substring(citation.LastIndexOf(" ")));
                }
            }
            //Try to find the paper information on pubmed
            HtmlAgilityPack.HtmlWeb pubmed = new HtmlAgilityPack.HtmlWeb();
            HtmlAgilityPack.HtmlDocument pubmedSearch = pubmed.Load("http://www.ncbi.nlm.nih.gov/pubmed?term=" + ConvertSpecialCharacters(paper.Title) + "[Title]");
            HtmlNodeCollection found = pubmedSearch.DocumentNode.SelectNodes("//div[@class='auths' and preceding-sibling::h1]");
            if (found != null && found.Count > 0 && (found.First<HtmlNode>().PreviousSibling.InnerText.Contains(paper.Title) || paper.Title.Contains(found.First<HtmlNode>().PreviousSibling.InnerText)))
            {
                paper.AuthorEngine = "PubMed";
                paper.Authors = found.First<HtmlNode>().InnerText;
            }
            else
            {
                //int flag = 0;
                //HtmlNodeCollection noResult = pubmedSearch.DocumentNode.SelectNodes("//ul[@class='messages']/li[@class='warn']");
                //if (noResult != null && noResult.Count > 0)
                //{
                //    HtmlNode node = noResult.First<HtmlNode>();
                //    if (node.InnerText.Contains("The following term was not found in PubMed"))
                //    {
                //        flag = 1;
                //    }
                //}
                HtmlNodeCollection someResults = pubmedSearch.DocumentNode.SelectNodes("//div[@class='sensor_content']/p/a");
                if (someResults != null)
                {
                    foreach (HtmlNode node in someResults)
                    {
                        if (node.InnerText.Contains(paper.Title) || paper.Title.Contains(node.InnerText) || RemoveStopWordsAndCompare(node.InnerText, paper.Title) >= 70)
                        {
                            HtmlAgilityPack.HtmlWeb pubmedSub = new HtmlAgilityPack.HtmlWeb();
                            HtmlAgilityPack.HtmlDocument pubmedSearchSub = pubmedSub.Load("http://www.ncbi.nlm.nih.gov" + node.Attributes["href"].Value.ToString());
                            HtmlNodeCollection foundSub = pubmedSearchSub.DocumentNode.SelectNodes("//div[@class='auths' and preceding-sibling::h1]");
                            if (foundSub != null && foundSub.Count > 0) // && (foundSub.First<HtmlNode>().PreviousSibling.InnerText.Contains(paper.Title) || paper.Title.Contains(foundSub.First<HtmlNode>().PreviousSibling.InnerText))
                            {
                                paper.AuthorEngine = "PubMed";
                                paper.Title = foundSub.First<HtmlNode>().PreviousSibling.InnerText;
                                paper.Authors = foundSub.First<HtmlNode>().InnerText;
                            }
                            break;
                        }
                    }
                }
            }
            return paper;
        }

        public string ReadFromFile(string fileName)
        {
            string html = "";
            if (!File.Exists(fileName))
            {
                MessageBox.Show("File " + fileName +" does not exist.");
                return null;
            }
            using (StreamReader sr = File.OpenText(fileName))
            {
                String input;
                while ((input = sr.ReadLine()) != null)
                {
                    html += input;
                }
                //Console.WriteLine("The end of the stream has been reached.");
            }
            return html;
        }

        public void ReadStopWords()
        {
            string fileName = ".\\stopwords.txt";
            string stopWords = "";
            if (!File.Exists(fileName))
            {
                MessageBox.Show("File " + fileName + " does not exist.");
                return;
            }
            using (StreamReader sr = File.OpenText(fileName))
            {
                String input;
                while ((input = sr.ReadLine()) != null)
                {
                    stopWords += input;
                }
                //Console.WriteLine("The end of the stream has been reached.");
            }
            string[] omitters = {", "};
            string[] words = stopWords.Split(omitters,StringSplitOptions.RemoveEmptyEntries);
            StopWordsList = new List<string>();
            foreach (string word in words)
                StopWordsList.Add(word);
            return;
        }

        public string RemoveNoise(string str)
        {
            if(str.Contains("&hellip;,"))
                str = str.Replace("&hellip;,","");
            if (str.Contains("&hellip;"))
                str = str.Replace("&hellip;", "");
            if (str.Contains("&amp;"))
                str = str.Replace("&amp;", "&");

            return str;
        }

        public string ConvertSpecialCharacters(string str)
        {
            if (str.Contains("&"))
                str = str.Replace("&", "%26");
            else if (str.Contains(":"))
                str = str.Replace(":","%3A");

            return str;
        }
        public float RemoveStopWordsAndCompare(string pubmedTitle, string scholarTitle)
        {
            List<string> PubmedTitleWords = RemoveStopWords(pubmedTitle);
            List<string> ScholarTitleWords = RemoveStopWords(scholarTitle);
            int matchedWords = 0, counter = 0;
            foreach (string word in PubmedTitleWords)
            {
                string[] str = new string[ScholarTitleWords.Count - matchedWords];
                ScholarTitleWords.CopyTo(matchedWords, str, 0, ScholarTitleWords.Count - matchedWords);
                
                if (str.Contains<string>(word))
                    matchedWords++;

                counter++;
            }
            if (matchedWords <= 0)
                return 0;
            else
                return ((float)matchedWords / (float)((PubmedTitleWords.Count > ScholarTitleWords.Count) ? PubmedTitleWords.Count : ScholarTitleWords.Count)) * 100;
        }

        public List<string> RemoveStopWords(string title)
        {
            string[] splitCharacters = { "," , " " , ":" , ";" , "&" , "."};
            string[] titleWords = title.Split(splitCharacters, StringSplitOptions.RemoveEmptyEntries);
            List<string> FinalWords = titleWords.ToList<string>();
            foreach (string word in titleWords)
            {
                if (StopWordsList.Contains(word))
                    FinalWords.Remove(word);
            }
            return FinalWords;
        }

        private void AuthorsByPapers_Click(object sender, EventArgs e)
        {
        }

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ExpertTabs.SelectedIndex == 1)
                GridPaperCount.DataSource = Publication_Helper.CountPublicationsByAuthors;
            else if (ExpertTabs.SelectedIndex == 2)
                cmbCitCategories.DataSource = Categories.DBCategories;
            else if (ExpertTabs.SelectedIndex == 3)
                cmbCoCategory.DataSource = Categories.DBCategories;
            else if (ExpertTabs.SelectedIndex == 4)
                cmbCategory.DataSource = Categories.DBCategories;
        }

        private void listAllPublicationsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AuthorPublicationsForm PubForm = new AuthorPublicationsForm();
            Publication_Helper helper = new Publication_Helper();
            helper.SelectedAuthor = GridPaperCount.SelectedRows[0].DataBoundItem as AuthorsByPublications;
            PubForm.AuthorPublicationList = helper.PublicationsOfAuthors;
            PubForm.ShowDialog();
        }

        private void GridPaperCount_CellMouseUp(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
                return;
            if (e.ColumnIndex < 0 || e.RowIndex < 0)
                return;
            selectedRow = e.RowIndex;
            GridPaperCount.Rows[e.RowIndex].Selected = true;
            contextMenuStrip1.Show(MousePosition);
        }

        private void btnFindExpert_Click(object sender, EventArgs e)
        {
            Thread splashthread = new Thread(new ThreadStart(SplashScreen.ShowSplashScreen));
            splashthread.IsBackground = true;
            splashthread.Start();

            Categories categories = cmbCategory.SelectedItem as Categories;
            List<Category_Expert> ExpertList = new List<Category_Expert>();
            try
            {
                DataRowCollection ExpertDataRowCollection;
                CDatabase db = new CDatabase(false);
                DataSet ExpertDS;
                db.BindInParameter("@category_id", categories.CategoryID, SqlDbType.Int);
                ExpertDS = db.ExecuteDataSetSP("sp_Category_Expert");
                if (ExpertDS != null)
                {
                    ExpertDataRowCollection = ExpertDS.Tables[0].Rows;
                    ExpertList.Clear();
                    foreach (DataRow ExpertDataRow in ExpertDataRowCollection)
                    {
                        ExpertList.Add(new Category_Expert(ExpertDataRow));
                    }
                }
                db.CloseDbConnection();
            }
            catch (Exception dbEx)
            {
                CMessage.ShowError(dbEx.Message);
                GridCategory.DataSource = null;
                return;
            }
            
            GridCategory.DataSource = ExpertList;
            SplashScreen.CloseSplashScreen();
        }

        private void GridCategory_CellMouseUp(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
                return;
            if (e.ColumnIndex < 0 || e.RowIndex < 0)
                return;
            GridCategory.Rows[e.RowIndex].Selected = true;
            ExpertMenu.Show(MousePosition);
        }

        private void DetailItem_Click(object sender, EventArgs e)
        {
            ExpertDetailForm ExpertForm = new ExpertDetailForm();

            if (ExpertTabs.SelectedIndex == 3)
            {
                AuthorsByCoAuthors auth = gridCoAuthors.SelectedRows[0].DataBoundItem as AuthorsByCoAuthors;
                ExpertForm.AuthorID = auth.AuthorID;
                ExpertForm.CategoryID = auth.CategoryID;
            }
            else
            {
                Category_Expert auth = GridCategory.SelectedRows[0].DataBoundItem as Category_Expert;
                ExpertForm.AuthorID = auth.AuthorID;
                ExpertForm.CategoryID = auth.CategoryID;
            }

            List<ExpertDetail> ExpertList = new List<ExpertDetail>();
            try
            {
                DataRowCollection ExpertDataRowCollection;
                CDatabase db = new CDatabase(false);
                DataSet ExpertDS;
                db.BindInParameter("@author_id", ExpertForm.AuthorID, SqlDbType.Int);
                db.BindInParameter("@category_id", ExpertForm.CategoryID, SqlDbType.Int);
                ExpertDS = db.ExecuteDataSetSP("sp_Expert_Detail");
                if (ExpertDS != null)
                {
                    ExpertDataRowCollection = ExpertDS.Tables[0].Rows;
                    ExpertList.Clear();
                    foreach (DataRow ExpertDataRow in ExpertDataRowCollection)
                    {
                        ExpertList.Add(new ExpertDetail(ExpertDataRow));
                    }
                }
                db.CloseDbConnection();
            }
            catch (Exception dbEx)
            {
                CMessage.ShowError(dbEx.Message);
                ExpertForm.ExpertDataSource = null;
                return;
            }
            ExpertForm.ExpertDataSource = ExpertList;
            ExpertForm.ShowDialog();
        }

        private void btnGO_Click(object sender, EventArgs e)
        {
            Citation_Helper citation = new Citation_Helper();
            citation.SelectedCategory = cmbCitCategories.SelectedItem as Categories;
            GridCitations.DataSource = citation.AuthorsCitations;
        }

        private void btnGoCoAuth_Click(object sender, EventArgs e)
        {
            CoAuthor_Helper coauthors = new CoAuthor_Helper();
            coauthors.SelectedCategory = cmbCoCategory.SelectedItem as Categories;
            gridCoAuthors.DataSource = coauthors.AuthorsCoAuthors;
        }

        private void gridCoAuthors_CellMouseUp(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
                return;
            if (e.ColumnIndex < 0 || e.RowIndex < 0)
                return;
            gridCoAuthors.Rows[e.RowIndex].Selected = true;
            ExpertMenu.Show(MousePosition);
        }

        private void GridPaperCount_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            listAllPublicationsToolStripMenuItem_Click(sender, EventArgs.Empty);
        }

        private void gridCoAuthors_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            DetailItem_Click(sender, EventArgs.Empty);
        }

        private void GridCategory_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            DetailItem_Click(sender, EventArgs.Empty);
        }

        public int NumOfRecPerPage
        {
            get { return NumOfRec; }
            set { NumOfRec = value; }
        }

        private void exportToExcelMenuItem_Click(object sender, EventArgs e)
        {
            if (GridCategory.RowCount <= 0)
            {
                CMessage.ShowError("Nothing to export");
                return;
            }

            ProgressForm pForm = new ProgressForm();
            pForm.Show();
            pForm.SetProgress(5);

            Cursor.Current = Cursors.WaitCursor;

            CDataGridExport oDataGridExport = new CDataGridExport();
            oDataGridExport.initializeExcelApplication();
            pForm.SetProgress(pForm.ProgressBar.Value + 10);

            Excel.Worksheet xlWorkSheet = oDataGridExport.addNewExcelWorkSheet("Expert sheet", 0);
            pForm.SetProgress(pForm.ProgressBar.Value + 10);

            oDataGridExport.writeDataGridIntoExcel(xlWorkSheet, GridCategory);

            xlWorkSheet.get_Range(xlWorkSheet.Cells[1, 1], xlWorkSheet.Cells[1, 2]).ColumnWidth = 15;
            xlWorkSheet.get_Range(xlWorkSheet.Cells[1, 3], xlWorkSheet.Cells[1, 5]).ColumnWidth = 20;
            xlWorkSheet.get_Range(xlWorkSheet.Cells[1, 6], xlWorkSheet.Cells[1, 6]).ColumnWidth = 30;
            xlWorkSheet.get_Range(xlWorkSheet.Cells[1, 7], xlWorkSheet.Cells[1, 8]).ColumnWidth = 15;
            xlWorkSheet.get_Range(xlWorkSheet.Cells[1, 1], xlWorkSheet.Cells[1, 1]).EntireRow.Insert(Excel.XlInsertShiftDirection.xlShiftDown, System.Type.Missing);
            xlWorkSheet.get_Range(xlWorkSheet.Cells[3, 2], xlWorkSheet.Cells[GridCategory.RowCount + 2, 2]).HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;

            Excel.Range firstRow = xlWorkSheet.get_Range(xlWorkSheet.Cells[1, 1], xlWorkSheet.Cells[1, 8]);
            Excel.Range secondRow = xlWorkSheet.get_Range(xlWorkSheet.Cells[2, 1], xlWorkSheet.Cells[2, 8]);

            firstRow.EntireRow.RowHeight = 36;
            firstRow.Font.Bold = true;
            firstRow.Font.Italic = true;
            firstRow.Font.Size = 16;
            firstRow.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
            firstRow.VerticalAlignment = Excel.XlVAlign.xlVAlignCenter;
            secondRow.Font.Size = 12;
            secondRow.RowHeight = 21;
            secondRow.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;

            xlWorkSheet.get_Range(xlWorkSheet.Cells[1, 1], xlWorkSheet.Cells[1, 8]).Merge(true);
            firstRow.Interior.Color = System.Drawing.Color.Gray.ToArgb();
            secondRow.Interior.Color = System.Drawing.Color.Gray.ToArgb();
            firstRow.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
            secondRow.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

            xlWorkSheet.Cells[1, 1] = "Experts for " + (cmbCategory.SelectedItem as Categories).CategoryName;

            pForm.SetProgress(pForm.ProgressBar.Value + 50);

            oDataGridExport.saveExcelApplication();
            pForm.SetProgress(pForm.ProgressBar.Value + 10);

            oDataGridExport.releaseObject(xlWorkSheet);
            pForm.SetProgress(pForm.ProgressBar.Value + 5);

            oDataGridExport.releaseApplicationObject();
            pForm.SetProgress(pForm.ProgressBar.Value + 5, "Completed....");

            pForm.Dispose();
            Cursor.Current = Cursors.Default;
            String strMessage = "Data exported successfully in the file " + oDataGridExport.strFilePath;
            CMessage.ShowInformation(strMessage);
        }

        //private CookieCollection GetCookies()
        //{
        //    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(String.Format("http://scholar.google.com.pk/scholar?q=bio+informatics&num=20&hl=en&btnG=Search&as_sdt=1%2C5&as_sdtp=on"));
        //    request.CookieContainer = new CookieContainer();
        //    HttpWebResponse response = (HttpWebResponse)request.GetResponse();
        //    Stream responseStream = response.GetResponseStream();
        //    return response.Cookies;
        //}

        //private void PostWithCookies()
        //{
        //    CookieCollection cookies = this.GetCookies();
        //    var request = (HttpWebRequest)WebRequest.Create(String.Format("http://scholar.google.com.pk/scholar?q=bio+informatics&num=20&hl=en&btnG=Search&as_sdt=1%2C5&as_sdtp=on"));
        //    request.CookieContainer = new CookieContainer();
        //    request.Method = "POST";
        //    request.ContentType = "application/x-www-form-urlencoded";
        //    string postData = String.Format("text={0}", String.IsNullOrEmpty(textBoxPost.Text)
        //        ? "somerandomemail@address.com" : this.textBoxPost.Text);
        //    byte[] bytes = Encoding.UTF8.GetBytes(postData);
        //    request.ContentLength = bytes.Length;
        //    if (cookies != null)
        //    {
        //        request.CookieContainer.Add(cookies);
        //    }

        //    Stream requestStream = request.GetRequestStream();
        //    requestStream.Write(bytes, 0, bytes.Length);

        //    var response = request.GetResponse();
        //    var stream = response.GetResponseStream();
        //    var reader = new StreamReader(stream);
        //    this.richTextBox1.Text = reader.ReadToEnd();
        //}
    }
}
