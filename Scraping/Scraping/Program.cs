﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace Scrapping
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            CMessage.Action = "Cancel";
            CMessage.Mode = "Offline";
            Application.Run(new StartForm());
            if (CMessage.Action == "OK")
                Application.Run(new ScrappingForm());
        }
    }
}
