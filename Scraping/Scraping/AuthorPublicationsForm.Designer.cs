﻿namespace Scrapping
{
    partial class AuthorPublicationsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.GridAuthPub = new System.Windows.Forms.DataGridView();
            this.authorIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.authorNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.paperIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.paperTitleDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.publicationsOfAuthorBindingSource = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.GridAuthPub)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.publicationsOfAuthorBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // GridAuthPub
            // 
            this.GridAuthPub.AllowUserToAddRows = false;
            this.GridAuthPub.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.LightGreen;
            this.GridAuthPub.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.GridAuthPub.AutoGenerateColumns = false;
            this.GridAuthPub.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.GridAuthPub.BackgroundColor = System.Drawing.Color.White;
            this.GridAuthPub.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.GridAuthPub.ColumnHeadersHeight = 41;
            this.GridAuthPub.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.GridAuthPub.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.authorIDDataGridViewTextBoxColumn,
            this.authorNameDataGridViewTextBoxColumn,
            this.paperIDDataGridViewTextBoxColumn,
            this.paperTitleDataGridViewTextBoxColumn});
            this.GridAuthPub.DataSource = this.publicationsOfAuthorBindingSource;
            this.GridAuthPub.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GridAuthPub.Location = new System.Drawing.Point(0, 0);
            this.GridAuthPub.MultiSelect = false;
            this.GridAuthPub.Name = "GridAuthPub";
            this.GridAuthPub.ReadOnly = true;
            this.GridAuthPub.RowHeadersVisible = false;
            this.GridAuthPub.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.GridAuthPub.Size = new System.Drawing.Size(685, 405);
            this.GridAuthPub.TabIndex = 20;
            // 
            // authorIDDataGridViewTextBoxColumn
            // 
            this.authorIDDataGridViewTextBoxColumn.DataPropertyName = "AuthorID";
            this.authorIDDataGridViewTextBoxColumn.HeaderText = "AuthorID";
            this.authorIDDataGridViewTextBoxColumn.Name = "authorIDDataGridViewTextBoxColumn";
            this.authorIDDataGridViewTextBoxColumn.ReadOnly = true;
            this.authorIDDataGridViewTextBoxColumn.Width = 74;
            // 
            // authorNameDataGridViewTextBoxColumn
            // 
            this.authorNameDataGridViewTextBoxColumn.DataPropertyName = "AuthorName";
            this.authorNameDataGridViewTextBoxColumn.HeaderText = "AuthorName";
            this.authorNameDataGridViewTextBoxColumn.Name = "authorNameDataGridViewTextBoxColumn";
            this.authorNameDataGridViewTextBoxColumn.ReadOnly = true;
            this.authorNameDataGridViewTextBoxColumn.Width = 91;
            // 
            // paperIDDataGridViewTextBoxColumn
            // 
            this.paperIDDataGridViewTextBoxColumn.DataPropertyName = "PaperID";
            this.paperIDDataGridViewTextBoxColumn.HeaderText = "PaperID";
            this.paperIDDataGridViewTextBoxColumn.Name = "paperIDDataGridViewTextBoxColumn";
            this.paperIDDataGridViewTextBoxColumn.ReadOnly = true;
            this.paperIDDataGridViewTextBoxColumn.Width = 71;
            // 
            // paperTitleDataGridViewTextBoxColumn
            // 
            this.paperTitleDataGridViewTextBoxColumn.DataPropertyName = "PaperTitle";
            this.paperTitleDataGridViewTextBoxColumn.HeaderText = "PaperTitle";
            this.paperTitleDataGridViewTextBoxColumn.Name = "paperTitleDataGridViewTextBoxColumn";
            this.paperTitleDataGridViewTextBoxColumn.ReadOnly = true;
            this.paperTitleDataGridViewTextBoxColumn.Width = 80;
            // 
            // publicationsOfAuthorBindingSource
            // 
            this.publicationsOfAuthorBindingSource.DataSource = typeof(Scrapping.Classes.PublicationsOfAuthor);
            // 
            // AuthorPublicationsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(685, 405);
            this.Controls.Add(this.GridAuthPub);
            this.Name = "AuthorPublicationsForm";
            this.Text = "Publications of Author";
            ((System.ComponentModel.ISupportInitialize)(this.GridAuthPub)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.publicationsOfAuthorBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView GridAuthPub;
        private System.Windows.Forms.DataGridViewTextBoxColumn authorIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn authorNameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn paperIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn paperTitleDataGridViewTextBoxColumn;
        private System.Windows.Forms.BindingSource publicationsOfAuthorBindingSource;

    }
}