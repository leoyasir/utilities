﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Scrapping
{
    public partial class StartForm : Form
    {
        public StartForm()
        {
            InitializeComponent();
            radioButton1_CheckedChanged(this, EventArgs.Empty);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            CMessage.Action = "Cancel";
            this.Close();
        }

        private void btnContinue_Click(object sender, EventArgs e)
        {
            if (radioButton1.Checked)
                CMessage.Mode = radioButton1.Text;
            else
                CMessage.Mode = radioButton2.Text;
            CMessage.Action = "OK";
            if (radioButton2.Checked && txtPath.Text == "")
            {
                CMessage.ShowError("Please select folder containing web pages for offline mode");
                return;
            }
            else if (radioButton2.Checked && txtPath.Text != "")
                CMessage.Path = txtPath.Text;
            this.Close();
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton2.Checked)
            {
                btnBrowse.Visible = true;
                txtPath.Visible = true;
            }
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton1.Checked)
            {
                btnBrowse.Visible = false;
                txtPath.Visible = false;
            }
        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            DialogResult result = folderBrowserDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                txtPath.Text = folderBrowserDialog1.SelectedPath;
            }
        }
    }
}
